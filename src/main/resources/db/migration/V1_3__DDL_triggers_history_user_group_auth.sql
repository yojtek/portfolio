DROP TRIGGER IF EXISTS authorities_history_trigger on authorities;

CREATE OR REPLACE FUNCTION authorities_history() RETURNS trigger AS $authorities_history$
    DECLARE
        "order" INTEGER;
    BEGIN
          IF TG_OP = 'INSERT'
          THEN
                SELECT COUNT(*) INTO "order"
                    FROM authorities_h
                    WHERE authority_id = NEW.authority_id;
                INSERT INTO authorities_h(
                            authority_id, authority_hid, authority_name, modification_type,
                            modification_time)
                    VALUES (NEW.authority_id, "order" + 1, NEW.authority_name, 'INSERT',
                            CURRENT_TIMESTAMP);
                RETURN NEW;

          ELSEIF TG_OP = 'UPDATE'
          THEN
                SELECT COUNT(*) INTO "order"
                    FROM authorities_h
                    WHERE authority_id = OLD.authority_id;
                INSERT INTO authorities_h(
                            authority_id, authority_hid, authority_name, modification_type,
                            modification_time)
                    VALUES (NEW.authority_id, "order" + 1, NEW.authority_name, 'UPDATE',
                            CURRENT_TIMESTAMP);
                RETURN NEW;

          ELSEIF TG_OP = 'DELETE'
          THEN
                SELECT COUNT(*) INTO "order"
                    FROM authorities_h
                    WHERE authority_id = OLD.authority_id;
                INSERT INTO authorities_h(
                            authority_id, authority_hid, authority_name, modification_type,
                            modification_time)
                    VALUES (OLD.authority_id, "order" + 1, OLD.authority_name, 'DELETE',
                            CURRENT_TIMESTAMP);
                RETURN OLD;
          END IF;
    END;
$authorities_history$ LANGUAGE plpgsql;

CREATE TRIGGER authorities_history_trigger
    BEFORE INSERT OR UPDATE OR DELETE ON authorities
    FOR EACH ROW
    EXECUTE PROCEDURE authorities_history();


DROP TRIGGER IF EXISTS group_authorities_history_trigger on group_authorities;

CREATE OR REPLACE FUNCTION group_authorities_history() RETURNS trigger AS $group_authorities_history_trigger$
    DECLARE
        "order" INTEGER;
    BEGIN
          IF TG_OP = 'INSERT'
          THEN
                SELECT COUNT(*) INTO "order"
                    FROM group_authorities_h
                    WHERE group_authority_id = NEW.group_authority_id;
                INSERT INTO group_authorities_h(
                            group_authority_id, group_authority_hid, group_id, authority_id,
                            modification_type, modification_time)
                    VALUES (NEW.group_authority_id, "order" + 1, NEW.group_id, NEW.authority_id,
                            'INSERT', CURRENT_TIMESTAMP);
                RETURN NEW;

          ELSEIF TG_OP = 'UPDATE'
          THEN
                SELECT COUNT(*) INTO "order"
                    FROM group_authorities_h
                    WHERE group_authority_id = OLD.group_authority_id;
                INSERT INTO group_authorities_h(
                            group_authority_id, group_authority_hid, group_id, authority_id,
                            modification_type, modification_time)
                    VALUES (NEW.group_authority_id, "order" + 1, NEW.group_id, NEW.authority_id,
                            'UPDATE', CURRENT_TIMESTAMP);
                RETURN NEW;

          ELSEIF TG_OP = 'DELETE'
          THEN
                SELECT COUNT(*) INTO "order"
                    FROM group_authorities_h
                    WHERE group_authority_id = OLD.group_authority_id;
                INSERT INTO group_authorities_h(
                            group_authority_id, group_authority_hid, group_id, authority_id,
                            modification_type, modification_time)
                    VALUES (OLD.group_authority_id, "order" + 1, OLD.group_id, OLD.authority_id,
                            'DELETE', CURRENT_TIMESTAMP);
                RETURN OLD;
          END IF;
    END;
$group_authorities_history_trigger$ LANGUAGE plpgsql;

CREATE TRIGGER group_authorities_history_trigger
    BEFORE INSERT OR UPDATE OR DELETE ON group_authorities
    FOR EACH ROW
    EXECUTE PROCEDURE group_authorities_history();


DROP TRIGGER IF EXISTS group_members_history_trigger on group_members;

CREATE OR REPLACE FUNCTION group_members_history() RETURNS trigger AS $group_members_history_trigger$
    DECLARE
        "order" INTEGER;
    BEGIN
          IF TG_OP = 'INSERT'
          THEN
                SELECT COUNT(*) INTO "order"
                    FROM group_members_h
                    WHERE group_member_id = NEW.group_member_id;
                INSERT INTO group_members_h(
                            group_member_id, group_member_hid, group_id, user_id,
                            modification_type, modification_time)
                    VALUES (NEW.group_member_id, "order" + 1, NEW.group_id, NEW.user_id,
                            'INSERT', CURRENT_TIMESTAMP);
                RETURN NEW;

          ELSEIF TG_OP = 'UPDATE'
          THEN
                SELECT COUNT(*) INTO "order"
                    FROM group_members_h
                    WHERE group_member_id = OLD.group_member_id;
                INSERT INTO group_members_h(
                            group_member_id, group_member_hid, group_id, user_id,
                            modification_type, modification_time)
                    VALUES (NEW.group_member_id, "order" + 1, NEW.group_id, NEW.user_id,
                            'UPDATE', CURRENT_TIMESTAMP);
                RETURN NEW;

          ELSEIF TG_OP = 'DELETE'
          THEN
                SELECT COUNT(*) INTO "order"
                    FROM group_members_h
                    WHERE group_member_id = OLD.group_member_id;
                INSERT INTO group_members_h(
                            group_member_id, group_member_hid, group_id, user_id,
                            modification_type, modification_time)
                    VALUES (OLD.group_member_id, "order" + 1, OLD.group_id, OLD.user_id,
                            'DELETE', CURRENT_TIMESTAMP);
                RETURN OLD;
          END IF;
    END;
$group_members_history_trigger$ LANGUAGE plpgsql;

CREATE TRIGGER group_members_history_trigger
    BEFORE INSERT OR UPDATE OR DELETE ON group_members
    FOR EACH ROW
    EXECUTE PROCEDURE group_members_history();


DROP TRIGGER IF EXISTS groups_history_trigger on groups;

CREATE OR REPLACE FUNCTION groups_history() RETURNS trigger AS $groups_history_trigger$
    DECLARE
        "order" INTEGER;
    BEGIN
          IF TG_OP = 'INSERT'
          THEN
                SELECT COUNT(*) INTO "order"
                    FROM groups_h
                    WHERE group_id = NEW.group_id;
                INSERT INTO groups_h(
                            group_id, group_hid, group_name, modification_type, modification_time)
                    VALUES (NEW.group_id, "order" + 1, NEW.group_name, 'INSERT', CURRENT_TIMESTAMP);
                RETURN NEW;

          ELSEIF TG_OP = 'UPDATE'
          THEN
                SELECT COUNT(*) INTO "order"
                    FROM groups_h
                    WHERE group_id = OLD.group_id;
                INSERT INTO groups_h(
                            group_id, group_hid, group_name, modification_type, modification_time)
                    VALUES (NEW.group_id, "order" + 1, NEW.group_name, 'UPDATE', CURRENT_TIMESTAMP);
                RETURN NEW;

          ELSEIF TG_OP = 'DELETE'
          THEN
                SELECT COUNT(*) INTO "order"
                    FROM groups_h
                    WHERE group_id = OLD.group_id;
                INSERT INTO groups_h(
                            group_id, group_hid, group_name, modification_type, modification_time)
                    VALUES (OLD.group_id, "order" + 1, OLD.group_name, 'DELETE', CURRENT_TIMESTAMP);
                RETURN OLD;
          END IF;
    END;
$groups_history_trigger$ LANGUAGE plpgsql;

CREATE TRIGGER groups_history_trigger
    BEFORE INSERT OR UPDATE OR DELETE ON groups
    FOR EACH ROW
    EXECUTE PROCEDURE groups_history();


DROP TRIGGER IF EXISTS users_history_trigger on users;

CREATE OR REPLACE FUNCTION users_history() RETURNS trigger AS $users_history_trigger$
    DECLARE
        "order" INTEGER;
    BEGIN
          IF TG_OP = 'INSERT'
          THEN
                SELECT COUNT(*) INTO "order"
                    FROM users_h
                    WHERE user_id = NEW.user_id;

                INSERT INTO users_h(
                            user_id, user_hid, username, password, enabled, email, firstname,
                            surname, sex, age, description, homepage, creation_time, modification_type,
                            modification_time)
                    VALUES (NEW.user_id, "order" + 1, NEW.username, NEW.password, NEW.enabled, NEW.email, NEW.firstname,
                            NEW.surname, NEW.sex, NEW.age, NEW.description, NEW.homepage, NEW.creation_time, 'INSERT',
                            CURRENT_TIMESTAMP);
                RETURN NEW;

          ELSEIF TG_OP = 'UPDATE'
          THEN
                SELECT COUNT(*) INTO "order"
                    FROM users_h
                    WHERE user_id = OLD.user_id;
                INSERT INTO users_h(
                            user_id, user_hid, username, password, enabled, email, firstname,
                            surname, sex, age, description, homepage, creation_time, modification_type,
                            modification_time)
                    VALUES (NEW.user_id, "order" + 1, NEW.username, NEW.password, NEW.enabled, NEW.email, NEW.firstname,
                            NEW.surname, NEW.sex, NEW.age, NEW.description, NEW.homepage, NEW.creation_time, 'UPDATE',
                            CURRENT_TIMESTAMP);
                RETURN NEW;

          ELSEIF TG_OP = 'DELETE'
          THEN
                SELECT COUNT(*) INTO "order"
                    FROM users_h
                    WHERE user_id = OLD.user_id;

                INSERT INTO users_h(
                            user_id, user_hid, username, password, enabled, email, firstname,
                            surname, sex, age, description, homepage, creation_time, modification_type,
                            modification_time)
                    VALUES (OLD.user_id, "order" + 1, OLD.username, OLD.password, OLD.enabled, OLD.email, OLD.firstname,
                            OLD.surname, OLD.sex, OLD.age, OLD.description, OLD.homepage, OLD.creation_time, 'DELETE',
                            CURRENT_TIMESTAMP);
                RETURN OLD;
          END IF;
    END;
$users_history_trigger$ LANGUAGE plpgsql;

CREATE TRIGGER users_history_trigger
    AFTER INSERT OR UPDATE OR DELETE ON users
    FOR EACH ROW
    EXECUTE PROCEDURE users_history();