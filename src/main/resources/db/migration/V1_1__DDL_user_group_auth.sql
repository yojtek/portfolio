CREATE TABLE users(
  user_id BIGSERIAL NOT NULL PRIMARY KEY,
  username VARCHAR(50) NOT NULL UNIQUE,
  password VARCHAR(100) NOT NULL,
  enabled BOOLEAN NOT NULL,
  email VARCHAR(16) NOT NULL UNIQUE,
  firstname VARCHAR(50),
  surname VARCHAR(50),
  sex VARCHAR(1),
  age INTEGER,
  description VARCHAR(500),
  homepage VARCHAR(50),
  creation_time TIMESTAMP
  );

CREATE TABLE groups (
  group_id BIGSERIAL PRIMARY KEY,
  group_name VARCHAR(50) NOT NULL UNIQUE
  );

CREATE TABLE authorities (
  authority_id BIGSERIAL PRIMARY KEY,
  authority_name VARCHAR(50) NOT NULL UNIQUE
  );

CREATE TABLE group_authorities (
  group_authority_id BIGSERIAL PRIMARY KEY,
  group_id BIGINT NOT NULL,
  authority_id BIGINT NOT NULL,
  CONSTRAINT group_authorities_group_id_fkey FOREIGN KEY(group_id) REFERENCES groups(group_id),
  CONSTRAINT group_authorities_authority_id_fkey FOREIGN KEY(authority_id) REFERENCES authorities(authority_id)
  );


CREATE TABLE group_members(
  group_member_id BIGSERIAL PRIMARY KEY,
  group_id BIGINT NOT NULL,
  user_id BIGINT NOT NULL,
  CONSTRAINT group_members_user_id_fkey FOREIGN KEY(user_id) REFERENCES users(user_id),
  CONSTRAINT group_members_group_id_fkey FOREIGN KEY(group_id) REFERENCES groups(group_id)
  );