CREATE TABLE users_h(
  user_id BIGINT,
  user_hid BIGSERIAL,
  username VARCHAR(50),
  password VARCHAR(100),
  enabled BOOLEAN,
  email VARCHAR(16),
  firstname VARCHAR(50),
  surname VARCHAR(50),
  sex VARCHAR(1),
  age INTEGER,
  description VARCHAR(500),
  homepage VARCHAR(50),
  creation_time TIMESTAMP,
  modification_type VARCHAR(20),
  modification_time TIMESTAMP,
  PRIMARY KEY (user_id, user_hid)
  );

CREATE TABLE groups_h (
  group_id BIGINT,
  group_hid BIGSERIAL,
  group_name VARCHAR(50),
  modification_type VARCHAR(20),
  modification_time TIMESTAMP,
  PRIMARY KEY (group_id, group_hid)
  );

CREATE TABLE authorities_h (
  authority_id BIGINT,
  authority_hid BIGSERIAL,
  authority_name VARCHAR(50),
  modification_type VARCHAR(20),
  modification_time TIMESTAMP,
  PRIMARY KEY (authority_id, authority_hid)
  );

CREATE TABLE group_authorities_h (
  group_authority_id BIGSERIAL,
  group_authority_hid BIGSERIAL,
  group_id BIGINT,
  authority_id BIGINT,
  modification_type VARCHAR(20),
  modification_time TIMESTAMP,
  PRIMARY KEY (group_authority_id, group_authority_hid)
  );


CREATE TABLE group_members_h(
  group_member_id BIGINT,
  group_member_hid BIGSERIAL,
  group_id BIGINT,
  user_id BIGINT,
  modification_type VARCHAR(20),
  modification_time TIMESTAMP,
  PRIMARY KEY (group_member_id, group_member_hid)
  );