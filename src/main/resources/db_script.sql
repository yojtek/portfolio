
CREATE ROLE portfolio LOGIN ENCRYPTED PASSWORD 'md55d26ada2cc5985fc900c312e44a28b90' -- password portfolio
  SUPERUSER CREATEDB CREATEROLE REPLICATION
   VALID UNTIL 'infinity';


   CREATE DATABASE portfolio_db
   WITH OWNER = portfolio
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       CONNECTION LIMIT = -1;
GRANT CONNECT, TEMPORARY ON DATABASE portfolio_db TO public;
GRANT ALL ON DATABASE portfolio_db TO portfolio;
