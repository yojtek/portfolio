'use strict';

var userService = angular.module('group.service', [])

userService.factory('Group', ['$resource',
        function($resource) {
      return $resource('api/group/:id/:authorities',
                      {id: '@id', authorities: '@authorities'});
    }]);

userService.factory('GroupLoader', ['Group', '$route', '$q',
    function(Group, $route, $q) {
  return function() {
    var delay = $q.defer();
    Group.get({id: $route.current.params.groupId}, function(group) {
      delay.resolve(group);
    }, function() {
      delay.reject('Unable to fetch group '  + $route.current.params.groupId);
    });
    return delay.promise;
  };
}]);