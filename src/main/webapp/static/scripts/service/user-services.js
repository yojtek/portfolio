'use strict';

var userService = angular.module('user.service', [])

userService.factory('User', ['$resource',
        function($resource) {
      return $resource('api/user/:id/:orderpattern/:currentpage/:pagesize',
                      {id: '@id', orderpattern: '@orderpattern', currentpage: '@currentpage', pagesize: '@pagesize'});
    }]);


userService.factory('UserLoader', ['User', '$route', '$q',
    function(User, $route, $q) {
  return function() {
    var delay = $q.defer();
    User.get({id: $route.current.params.userId}, function(user) {
      delay.resolve(user);
    }, function() {
      delay.reject('Unable to fetch user '  + $route.current.params.userId);
    });
    return delay.promise;
  };
}]);