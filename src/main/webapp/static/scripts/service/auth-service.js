'use strict';

var authService = angular.module('auth.service', []);

authService.factory('Auth', ['$resource', function($resource) {
      return $resource('api/auth/user',{});
}])

authService.factory('httpRequestInterceptor', ['$q', '$location', '$rootScope',
                    function($q, $location) {
 return {
    response: function(response){
      return promise.then(
        function success(response) {
        return response;
      },
      function error(response) {
        if(response.status === 403){
          $location.path('index.html#/view/user/login');
          return $q.reject(response);
        }
        else{
          return response;
        }
      });
    }
  }
}]);
