'use strict';

var userModule = angular.module('userModule', ['user.service']);

userModule.config(function($routeProvider) {
$routeProvider
    .when("/view/user/forgotten", {
            templateUrl: './views/user/forgottenPassword.html',
            controller: 'UserListController'
        })
    .when("/view/user/register", {
            templateUrl: './views/user/register.html',
            controller: 'UserRegisterController'
        })
    .when("/view/user/edit/:userId", {
                templateUrl: './views/user/edit.html',
                resolve: {
                          user: ["UserLoader", function(UserLoader) {
                            return UserLoader();
                          }]
                        },
                controller: 'UserEditController'
            })
    .when("/view/user/list", {
            templateUrl: './views/user/list.html',
            controller: 'UserListController',
        })
});
//TODO: hardcoded orderby and pagination
userModule.controller('UserListController', function ($scope, User){
    // members
    $scope.users = User.query({orderpattern: 'orderByNameDESC', currentpage: '1', pagesize: '20'})
    // functions
    $scope.removeUser = function(index){
        var user = $scope.users[index];
        User.remove({id: user.userId}, function success(response){
            if(response.responseResult == true){
                $scope.users = User.query({orderpattern: 'orderByNameDESC', currentpage: '1', pagesize: '20'});
            }else{
                $scope.msg = response.responseMessage;
            }
        }, function err(){})
        }
 })

userModule.controller('UserRegisterController', function ($scope, User){
     // members
     $scope.msg = '';
     $scope.authorityList = {};
     $scope.user = new User();
     var tempUser = new User();
     // functions
     $scope.registerUser = function(){
     $scope.user.$save(function(response){
         angular.copy($scope.user, tempUser);
         if(response.responseResult){
         $scope.msg = 'udalo się';
         $scope.user = new User();
         }else{
         $scope.msg = 'nie udalo sie bo ' + response.responseMessage;
         angular.copy(tempUser, $scope.user);
         }
     });
     }
     // initialisation
})

userModule.controller('UserEditController', function ($scope, User, user, Group){
    // members
    $scope.msg = '';
    $scope.groupList = {};
    var tempUser = new User();
    // functions
    $scope.addGroup = function(index){
             $scope.user.groupJsonList[$scope.user.groupJsonList.length] = $scope.groupList[index];
             $scope.groupList.splice(index, 1);
         }

    $scope.removeGroup = function(index){
             $scope.groupList[$scope.groupList.length] = $scope.user.groupJsonList[index];
             $scope.user.groupJsonList.splice(index, 1);
         }


    $scope.editUser = function(){
    angular.copy($scope.user, tempUser);
    $scope.user.$save({id: user.userId},function(response){
        if(response.responseResult){
        $scope.msg = 'udalo się';
        }else{
        $scope.msg = 'nie udalo sie bo ' + response.responseMessage;
        angular.copy(tempUser, $scope.user);
        }
    }, function(){
         $scope.msg = 'Nie oczekiwany błąd podczas edycji użytkownika';
         angular.copy(tempUser, $scope.user);
    });
    }
    // initialisation
    $scope.user = user;
    Group.query({}, function success(response){
             $scope.groupList = response;
                 if ($scope.user.groupJsonList == null ){
                     $scope.user.groupJsonList = [];
                 }
             for(var i = 0; i < $scope.groupList.length; i++){
                 var deleteGroupElement = false;
                 for(var j = 0; j < $scope.user.groupJsonList.length; j++){
                       if($scope.groupList[i].groupName === $scope.user.groupJsonList[j].groupName){
                            $scope.groupList.splice(i, 1);
                       }
                 }
              }
         }, function err(){}
    );


})

