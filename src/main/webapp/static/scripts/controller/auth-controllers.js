'use strict';

var authModule = angular.module('authModule', []);

userModule.config(function($routeProvider) {
$routeProvider
    .when("/view/user/login", {
        templateUrl: './views/user/login.html',
        controller: 'LoginController'
        })
    .when("/view/user/logout", {
             templateUrl: './views/main.html',
             controller: 'LogoutController'
         })
});

authModule.controller('LoginController', function ($scope, $rootScope, $http, $log) {
// members
$scope.authFailureStatus = false;
// functions
$scope.signIn = function () {
// post logging
$http.post('/process', "username="+ $scope.username + "&password="+ $scope.password, {
    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).
    success(function(status){
        $scope.authFailureStatus = false;
        Auth.get({}, function success(data, status){

            if(data.loggedUser != null){
                if(data.loggedUser === 'anonymousUser'){
                    $rootScope.loggedUser = 'Gość'
                    $rootScope.isUserLogged = false;
                }else{
                    $rootScope.loggedUser = data.loggedUser;
                    $rootScope.isUserLogged = true;
                }
            }else{
                $rootScope.loggedUser = 'Gość';
                $rootScope.isUserLogged = false;
            }
        var views = data.views;
        if(views != null){
            for (var i = 0; i < views.length; i++){
                if (views[i].match("admin_menu")){
                    $rootScope.viewAdminMenu = true;
                }
            }
        }
        }, function err(){});
    }).
    error(function(status) {
        $scope.authFailureStatus = true;
    });
};
})

authModule.controller('LogoutController', function ($scope, $location, $http, $rootScope) {
    $http.get('/logout').success(function(data){
        $rootScope.isUserLogged = false;
        $rootScope.viewAdminMenu = false;
        $rootScope.loggedUser = 'Gość';
    });
});



