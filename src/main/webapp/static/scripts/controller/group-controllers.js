'use strict';

var groupModule = angular.module('groupModule', ['group.service']);

groupModule.config(function($routeProvider) {
$routeProvider
    .when("/view/group/create", {
            templateUrl: './views/group/create.html',
            controller: 'GroupCreateController'
        })
    .when("/view/group/edit/:groupId", {
            templateUrl: './views/group/edit.html',
            resolve: {
                  group: ["GroupLoader", function(GroupLoader) {
                  return GroupLoader();
                  }]
            },
            controller: 'GroupEditController'
            })
    .when("/view/group/list", {
            templateUrl: './views/group/list.html',
            controller: 'GroupListController',
        })
});

groupModule.controller('GroupListController', function ($scope, Group){
    // members
    $scope.groups = Group.query({});

    // functions
    $scope.removeGroup = function(index){
    var group = $scope.groups[index];
    Group.remove({id: group.groupId}, function success(response){
        if(response.responseResult == true){
            $scope.groups = Group.query({});
        }else{
            $scope.msg = response.responseMessage;
        }
    }, function err(){})
    }
 })

groupModule.controller('GroupCreateController', function ($scope, Group){
    // members
    $scope.msg = '';
    $scope.authorityList = {};
    $scope.group = new Group({
       authorityJsonList: []
    });
    var tempGroup = new Group({
       authorityJsonList: []
    });

    // functions
    $scope.addAuthority = function(index){
        $scope.group.authorityJsonList[$scope.group.authorityJsonList.length] = $scope.authorityList[index];
        $scope.authorityList.splice(index, 1);
    }

    $scope.removeAuthority = function(index){
            $scope.authorityList[$scope.authorityList.length] = $scope.group.authorityJsonList[index];
            $scope.group.authorityJsonList.splice(index, 1);
        }

    $scope.createGroup = function(){
    angular.copy($scope.group, tempGroup);
    $scope.group.$save(function(response){
        if(response.responseResult){
        $scope.msg = 'Grupa została dodana!';
        $scope.group = new Group({
               authorityJsonList: []
            });
        }else{
        $scope.msg = 'Nie można dodać grupy, ponieważ: ' + response.responseMessage;
        angular.copy(tempGroup, $scope.group);
        }
    });
    }

    // initialisation
    Group.query({authorities: 'authorities'}, function success(response){
        $scope.authorityList = response;
    }, function err(){}
    );
 })

 groupModule.controller('GroupEditController', function ($scope, Group, group, $location){
     // members
     $scope.msg = '';
     $scope.authorityList = {};
     $scope.group = group;
       if ($scope.group.authorityJsonList == null ){
        $scope.group.authorityJsonList = [];
     }
     var tempGroup = new Group({
        authorityJsonList: []
     });


     // functions
     $scope.addAuthority = function(index){
         $scope.group.authorityJsonList[$scope.group.authorityJsonList.length] = $scope.authorityList[index];
         $scope.authorityList.splice(index, 1);
     }

     $scope.removeAuthority = function(index){
             $scope.authorityList[$scope.authorityList.length] = $scope.group.authorityJsonList[index];
             $scope.group.authorityJsonList.splice(index, 1);
         }

     $scope.editGroup = function(){
     angular.copy($scope.group, tempGroup);
     $scope.group.$save({id: group.groupId},function(response){
         if(response.responseResult){
         $scope.msg = 'Grupa została zaktualizowana!';
         }else{
         $scope.msg = 'Nie można zaktualizować grupy, ponieważ: '  + response.responseMessage;
         angular.copy(tempGroup, $scope.group);
         }
     });
     }

     // initialisation
     $scope.group = group;
     Group.query({authorities: 'authorities'}, function success(response){
         $scope.authorityList = response;
         if ($scope.group.authorityJsonList == null ){
             $scope.group.authorityJsonList = [];
         }
         for(var i = 0; i < $scope.authorityList.length; i++){
            for(var j = 0; j < $scope.group.authorityJsonList.length; j++){
                if($scope.authorityList[i].authorityName === $scope.group.authorityJsonList[j].authorityName){
                $scope.authorityList.splice(i, 1);
                }
            }
         }
     }, function err(){}
     );

  })