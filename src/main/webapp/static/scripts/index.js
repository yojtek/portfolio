'use strict';

var indexApp = angular.module('indexApp', ['ngRoute' , 'ngResource' ,'formUtils', 'portfolioModule', 'userModule', 'groupModule', 'authModule', 'auth.service']);

indexApp.config(function($routeProvider, $httpProvider) {
//$httpProvider.interceptors.push('httpRequestInterceptor');
$routeProvider.
    when("/view/main", {
        templateUrl: './views/main.html',
        controller: 'IndexController'
    })
    .otherwise({
        redirectTo: '/view/main'
    });
})

indexApp.run(function(Auth, $rootScope, $log){
    // members
    $rootScope.viewAdminMenu = false;
    // init user and views access
    Auth.get({}, function success(data){
        if(data.loggedUser != null){
          if(data.loggedUser === 'anonymousUser'){
            $rootScope.loggedUser = 'Gość'
            $rootScope.isUserLogged = false;
          }else{
            $rootScope.loggedUser = data.loggedUser;
            $rootScope.isUserLogged = true;
          }
        }else{
          $rootScope.loggedUser = 'Gość';
          $rootScope.isUserLogged = false;
        }
        var views = data.views;
        if(views != null){
            for (var i = 0; i < views.length; i++){
                if (views[i].match("admin_menu")){
                    $rootScope.viewAdminMenu = true;
                }
            }
        }
    }, function err(){
    });
})

indexApp.controller('IndexController', function ($scope){
});

