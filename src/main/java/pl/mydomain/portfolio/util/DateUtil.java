package pl.mydomain.portfolio.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat();

    public static Timestamp convertDateToTimestamp(Date date) {
        return new Timestamp(date.getTime());
    }

    public static Timestamp getCurrentTimestamp() {
        return new Timestamp(new Date().getTime());
    }

    public static String parseTimestampToString(Timestamp timestamp) {
        return simpleDateFormat.format(timestamp.getTime());
    }
}
