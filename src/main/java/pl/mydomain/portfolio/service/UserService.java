package pl.mydomain.portfolio.service;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import pl.mydomain.portfolio.domain.dao.GroupDao;
import pl.mydomain.portfolio.domain.dao.GroupMemberDao;
import pl.mydomain.portfolio.domain.dao.UserDao;
import pl.mydomain.portfolio.domain.entity.GroupMemberEntity;
import pl.mydomain.portfolio.domain.entity.UserEntity;
import pl.mydomain.portfolio.domain.mapper.UserMapper;
import pl.mydomain.portfolio.domain.pojo.GroupJson;
import pl.mydomain.portfolio.domain.pojo.JsonCover;
import pl.mydomain.portfolio.domain.pojo.UserJson;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserService {

    private static final String USERNAME_ALREADY_EXIST = "Użytkownik o tej nazwie już istnieje";
    private static final String USER_EMAIL_ALREADY_EXIST = "Adres email o tej nazwie już istnieje";
    private static final String USER_ID_NOT_EXIST = "Użytkownik nie istnieje";

    protected Logger logger = LoggerFactory.getLogger(UserService.class);
    @Autowired
    protected BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private UserDao userDao;
    @Autowired
    private GroupDao groupDao;
    @Autowired
    private GroupMemberDao groupMemberDao;
    @Autowired
    private GroupAuthorityService groupAuthorityService;

    private UserMapper userMapper = new UserMapper();

    public JsonCover createUser(UserJson userJson) {
        JsonCover jsonCover = new JsonCover();
        if (userDao.findByUsername(userJson.getUsername()) == null) {
            if (userDao.findByEmail(userJson.getEmail()) != null) {
                jsonCover.setResponseResult(false);
                jsonCover.setResponseMessage(USER_EMAIL_ALREADY_EXIST);
                return jsonCover;
            }
            UserEntity userEntity = new UserEntity(userJson.getUsername(), bCryptPasswordEncoder.encode(userJson.getPassword()),
                    userJson.getEnabled(), userJson.getEmail(), userJson.getFirstname(), userJson.getSurname(), userJson.getSex(),
                    userJson.getAge(), userJson.getDescription(), userJson.getHomepage(), userJson.getCreationTime(), null);
            userEntity.setEnabled(true);
            // id 23L is for group "newUser" TODO: need to find out better way to solve it
            groupMemberDao.save(new GroupMemberEntity(groupDao.findOne(23L), userEntity));
            userDao.save(userEntity);
            jsonCover.setResponseResult(true);
        } else {
            jsonCover.setResponseResult(false);
            jsonCover.setResponseMessage(USERNAME_ALREADY_EXIST);
        }
        return jsonCover;
    }

    @Transactional
    public JsonCover updateUser(Long id, UserJson userJson) {
        JsonCover jsonCover = new JsonCover();
        UserEntity userEntity = userDao.findOne(id);
        if (userEntity != null) {
            if (!StringUtils.isEmpty(userJson.getPassword())) {
                userEntity.setPassword(bCryptPasswordEncoder.encode(userJson.getPassword()));
            }
            if (!StringUtils.isEmpty(userJson.getEmail())) {
                UserEntity userEntityMail = userDao.findByEmail(userJson.getEmail());
                if (userEntityMail == null || userEntityMail.getUserId().equals(userEntity.getUserId())) {
                    userEntity.setEmail(userJson.getEmail());
                } else {
                    jsonCover.setResponseResult(false);
                    jsonCover.setResponseMessage(USER_EMAIL_ALREADY_EXIST);
                    return jsonCover;
                }
            }
            userEntity.setFirstname(userJson.getFirstname());
            userEntity.setSurname(userJson.getSurname());
            userEntity.setHomepage(userJson.getHomepage());
            userEntity.setAge(userJson.getAge());
            userEntity.setDescription(userJson.getDescription());
            userEntity.setSex(userJson.getSex());
            userEntity.setEnabled(userJson.getEnabled());
            if (userJson.getGroupJsonList() != null && !userJson.getGroupJsonList().isEmpty()) {
                Set<GroupMemberEntity> groupMemberEntitySet = userEntity.getGroupMemberEntities();
                Set<GroupMemberEntity> groupMemberEntitySetToRemove = new HashSet<>();
                List<GroupJson> groupJsonListToRemove = new ArrayList<>();
                if (!groupMemberEntitySet.isEmpty()) {
                    for (GroupMemberEntity groupMemberEntity : groupMemberEntitySet) {
                        GroupMemberEntity groupMemberEntityToRemove = groupMemberEntity;
                        for (GroupJson groupJson : userJson.getGroupJsonList()) {
                            if (groupMemberEntity.getGroupEntity().getGroupId().compareTo(groupJson.getGroupId()) == 0) {
                                groupMemberEntityToRemove = null;
                                groupJsonListToRemove.add(groupJson);
                                break;
                            }
                        }
                        if (groupMemberEntityToRemove != null) {
                            groupMemberEntitySetToRemove.add(groupMemberEntityToRemove);
                        }
                    }
                    if (!groupJsonListToRemove.isEmpty()) {
                        userJson.getGroupJsonList().removeAll(groupJsonListToRemove);
                    }
                    if (!groupMemberEntitySetToRemove.isEmpty()) {
                        groupMemberEntitySet.removeAll(groupMemberEntitySetToRemove);
                    }
                }
                for (GroupJson groupJson : userJson.getGroupJsonList()) {
                    groupMemberEntitySet.add(new GroupMemberEntity(groupDao.findOne(groupJson.getGroupId()), userEntity));
                }
            } else {
                Set<GroupMemberEntity> groupMemberEntitySet = userEntity.getGroupMemberEntities();
                groupMemberEntitySet.clear();
            }
            userDao.save(userEntity);
            jsonCover.setResponseResult(true);
        } else {
            jsonCover.setResponseResult(false);
            jsonCover.setResponseMessage(USER_ID_NOT_EXIST);
        }
        return jsonCover;
    }

    public JsonCover deleteUser(Long userId) {
        JsonCover jsonCover = new JsonCover();
        UserEntity userEntity = userDao.findOne(userId);
        if (userEntity != null) {
            userDao.delete(userEntity);
            jsonCover.setResponseResult(true);
        } else {
            jsonCover.setResponseResult(false);
            jsonCover.setResponseMessage(USER_ID_NOT_EXIST);
        }
        return jsonCover;
    }

    public List<UserJson> getAllUsers(String orderPattern, Integer currentPageNumber,
                                      Integer pageSize) {
        List<UserEntity> userEntityList = userDao.getAll(orderPattern, convertPageNumberToOffset(currentPageNumber, pageSize), pageSize);
        List<UserJson> userJsonList = new ArrayList<>();
        if (userEntityList != null && !userEntityList.isEmpty()) {
            for (UserEntity userEntity : userEntityList) {
                UserJson userJson;
                userJson = userMapper.convertUserEntityToJson(userEntity);
                if (userEntity.getGroupMemberEntities() != null && !userEntity.getGroupMemberEntities().isEmpty()) {
                    userJson.setGroupJsonList(convertGroupEntityListToJsonList(userEntity.getGroupMemberEntities()));
                }
                userJsonList.add(userJson);
            }
        }
        return userJsonList;
    }

    public UserJson getUser(Long userId) {
        UserEntity userEntity = userDao.findOne(userId);
        UserJson userJson = null;
        if (userEntity != null) {
            userJson = userMapper.convertUserEntityToJson(userEntity);
            if (userEntity.getGroupMemberEntities() != null && !userEntity.getGroupMemberEntities().isEmpty()) {
                userJson.setGroupJsonList(convertGroupEntityListToJsonList(userEntity.getGroupMemberEntities()));
            }
            userJson.setPassword(null);
        }
        return userJson;
    }


    private List<GroupJson> convertGroupEntityListToJsonList(Set<GroupMemberEntity> groupMemberEntitySet) {
        List<GroupJson> groupJsonList = new ArrayList<>();
        if (groupMemberEntitySet != null && !groupMemberEntitySet.isEmpty()) {
            for (GroupMemberEntity groupMemberEntity : groupMemberEntitySet) {
                GroupJson groupJson = new GroupJson();
                groupJson.setGroupId(groupMemberEntity.getGroupEntity().getGroupId());
                groupJson.setGroupName(groupMemberEntity.getGroupEntity().getGroupName());
                groupJsonList.add(groupJson);
            }
        }
        return groupJsonList;
    }

    private int convertPageNumberToOffset(int currentPageNumber, int pageSize) {
        return (currentPageNumber - 1) * pageSize;
    }
}
