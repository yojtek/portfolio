package pl.mydomain.portfolio.service;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.mydomain.portfolio.domain.dao.AuthorityDao;
import pl.mydomain.portfolio.domain.dao.GroupAuthorityDao;
import pl.mydomain.portfolio.domain.dao.GroupDao;
import pl.mydomain.portfolio.domain.entity.GroupAuthorityEntity;
import pl.mydomain.portfolio.domain.entity.GroupEntity;
import pl.mydomain.portfolio.domain.entity.GroupMemberEntity;
import pl.mydomain.portfolio.domain.entity.UserEntity;
import pl.mydomain.portfolio.domain.mapper.GroupAuthorityMapper;
import pl.mydomain.portfolio.domain.pojo.AuthorityJson;
import pl.mydomain.portfolio.domain.pojo.GroupJson;
import pl.mydomain.portfolio.domain.pojo.JsonCover;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class GroupAuthorityService {

    private static final String ERR_GROUP_NOT_FOUND = "Grupa nie została usunięta, ponieważ grupa już nie istnieje";
    private static final String ERR_GROUP_WITH_THIS_NAME_IS_ALREADY_EXIST = "Wystąpił błąd, grupa o podanej nazwie już istnieje";
    protected Logger logger = LoggerFactory.getLogger(GroupAuthorityService.class);
    @Autowired
    private AuthorityDao authorityDao;
    @Autowired
    private GroupDao groupDao;
    @Autowired
    private GroupAuthorityDao groupAuthorityDao;


    private GroupAuthorityMapper groupAuthorityMapper = new GroupAuthorityMapper();

    public JsonCover createGroup(GroupJson groupJson) {
        JsonCover jsonCover = new JsonCover();
        if (groupDao.findByGroupName(groupJson.getGroupName()) == null) {
            GroupEntity groupEntity = new GroupEntity();
            groupEntity.setGroupName(groupJson.getGroupName());
            groupDao.save(groupEntity);
            for (AuthorityJson authorityJson : groupJson.getAuthorityJsonList()) {
                groupAuthorityDao.save(new GroupAuthorityEntity(authorityDao.findOne(authorityJson.getAuthorityId()), groupEntity));
            }
            jsonCover.setResponseResult(true);
        } else {
            jsonCover.setResponseResult(false);
            jsonCover.setResponseMessage(ERR_GROUP_WITH_THIS_NAME_IS_ALREADY_EXIST);
        }
        return jsonCover;
    }

    public JsonCover updateGroup(Long id, GroupJson groupJson) {
        JsonCover jsonCover = new JsonCover();
        GroupEntity groupEntity = groupDao.findOne(id);
        if (groupEntity == null) {
            jsonCover.setResponseResult(false);
            jsonCover.setResponseMessage(ERR_GROUP_NOT_FOUND);
        } else {
            GroupEntity groupEntityCheckForName = groupDao.findByGroupName(groupJson.getGroupName());
            if (groupEntityCheckForName == null || (groupEntityCheckForName.getGroupId().compareTo(groupEntity.getGroupId()) == 0)) {
                groupEntity.setGroupName(groupJson.getGroupName());
                if (groupJson.getAuthorityJsonList() != null && !groupJson.getAuthorityJsonList().isEmpty()) {
                    Set<GroupAuthorityEntity> groupAuthorityEntitySet = groupEntity.getGroupAuthorityEntities();
                    if (!groupEntity.getGroupAuthorityEntities().isEmpty()) {
                        Set<GroupAuthorityEntity> groupAuthorityEntitySetToRemove = new HashSet<>();
                        List<AuthorityJson> authorityJsonListRepeated = new ArrayList<>();
                        GroupAuthorityEntity groupAuthorityEntityToRemove = null;
                        for (GroupAuthorityEntity groupAuthorityEntity : groupEntity.getGroupAuthorityEntities()) {
                            groupAuthorityEntityToRemove = groupAuthorityEntity;
                            for (AuthorityJson authorityJson : groupJson.getAuthorityJsonList()) {
                                if (groupAuthorityEntity.getAuthorityEntity().getAuthorityId().compareTo(authorityJson.getAuthorityId()) == 0) {
                                    groupAuthorityEntityToRemove = null;
                                    authorityJsonListRepeated.add(authorityJson);
                                    break;
                                }
                            }
                            if (groupAuthorityEntityToRemove != null) {
                                groupAuthorityEntitySetToRemove.add(groupAuthorityEntityToRemove);
                            }
                        }
                        groupAuthorityEntitySet.removeAll(groupAuthorityEntitySetToRemove);
                        groupJson.getAuthorityJsonList().removeAll(authorityJsonListRepeated);
                    }
                    for (AuthorityJson authorityJson : groupJson.getAuthorityJsonList()) {
                        groupAuthorityEntitySet.add(new GroupAuthorityEntity(authorityDao.findOne(authorityJson.getAuthorityId()), groupEntity));
                    }
                } else {
                    Set<GroupAuthorityEntity> groupAuthorityEntitySet = groupEntity.getGroupAuthorityEntities();
                    groupAuthorityEntitySet.clear();
                }
                groupDao.save(groupEntity);
                jsonCover.setResponseResult(true);
            } else {
                jsonCover.setResponseResult(false);
                jsonCover.setResponseMessage(ERR_GROUP_WITH_THIS_NAME_IS_ALREADY_EXIST);
            }
        }
        return jsonCover;
    }

    public JsonCover deleteGroupById(Long groupId) {
        JsonCover jsonCover = new JsonCover();
        GroupEntity groupEntity = groupDao.findOne(groupId);
        if (groupEntity == null) {
            jsonCover.setResponseResult(false);
            jsonCover.setResponseMessage(ERR_GROUP_NOT_FOUND);
        } else {
            groupDao.delete(groupEntity);
            jsonCover.setResponseResult(true);
        }
        return jsonCover;
    }

    public GroupJson getGroupById(Long groupId) {
        return groupAuthorityMapper.convertGroupEntityToJson(groupDao.findOne(groupId));
    }

    public List<GroupJson> getAllGroups() {
        return groupAuthorityMapper.convertGroupEntityListToJsonList(groupDao.findAll());
    }

    public List<GroupJson> getGroupsAndAuthoritiesForUser(UserEntity userEntity) {
        Set<GroupMemberEntity> groupMemberEntitySet = userEntity.getGroupMemberEntities();
        List<GroupJson> groupJsonList = new ArrayList<>();
        if (groupMemberEntitySet != null && !groupMemberEntitySet.isEmpty()) {
            groupJsonList.addAll(groupMemberEntitySet.stream().map(groupMemberEntity -> groupAuthorityMapper.convertGroupEntityToJson(groupMemberEntity.getGroupEntity())).collect(Collectors.toList()));
        } else {
            logger.warn("Trying convert groupMemberEntitySet to jsonList, but groupMemberEntitySet is NULL OR EMPTY !!");
        }
        return groupJsonList;
    }

    public List<AuthorityJson> getAllAuthorities() {
        return groupAuthorityMapper.convertAuthorityEntityListToJsonList(authorityDao.findAll());
    }

}

