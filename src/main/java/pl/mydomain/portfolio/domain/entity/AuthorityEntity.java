package pl.mydomain.portfolio.domain.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name = "authorities")
public class AuthorityEntity {

    private Long authorityId;
    private String authorityName;
    private Set<GroupAuthorityEntity> groupAuthorityEntities = new HashSet<>();

    public AuthorityEntity() {
    }

    public AuthorityEntity(Long authorityId, String authorityName, Set<GroupAuthorityEntity> groupAuthorityEntities) {
        this.authorityId = authorityId;
        this.authorityName = authorityName;
        this.groupAuthorityEntities = groupAuthorityEntities;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "authority_id", unique = true, nullable = false)
    public Long getAuthorityId() {
        return authorityId;
    }

    public void setAuthorityId(Long authorityId) {
        this.authorityId = authorityId;
    }

    @Column(name = "authority_name", unique = true, nullable = false)
    public String getAuthorityName() {
        return authorityName;
    }

    public void setAuthorityName(String authorityName) {
        this.authorityName = authorityName;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "authorityEntity", cascade = CascadeType.ALL)
    public Set<GroupAuthorityEntity> getGroupAuthorityEntities() {
        return groupAuthorityEntities;
    }

    public void setGroupAuthorityEntities(Set<GroupAuthorityEntity> groupAuthorityEntities) {
        this.groupAuthorityEntities = groupAuthorityEntities;
    }
}

