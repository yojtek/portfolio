package pl.mydomain.portfolio.domain.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "users")
public class UserEntity {

    private Long userId;
    private String username;
    private String password;
    private Boolean enabled;
    private String email;
    private String firstname;
    private String surname;
    private String sex;
    private Integer age;
    private String description;
    private String homepage;
    private Timestamp creationTime;
    private Set<GroupMemberEntity> groupMemberEntities = new HashSet<>();

    public UserEntity() {
    }

    public UserEntity(Long userId, String username, String password, Boolean enabled,
                      String email, String firstname, String surname, String sex,
                      Integer age, String description, String homepage,
                      Timestamp creationTime, Set<GroupMemberEntity> groupMemberEntities) {
        this.userId = userId;
        this.username = username;
        this.password = password;
        this.enabled = enabled;
        this.email = email;
        this.firstname = firstname;
        this.surname = surname;
        this.sex = sex;
        this.age = age;
        this.description = description;
        this.homepage = homepage;
        this.creationTime = creationTime;
        this.groupMemberEntities = groupMemberEntities;
    }

    public UserEntity(String username, String password, Boolean enabled,
                      String email, String firstname, String surname, String sex,
                      Integer age, String description, String homepage,
                      Timestamp creationTime, Set<GroupMemberEntity> groupMemberEntities) {
        this.username = username;
        this.password = password;
        this.enabled = enabled;
        this.email = email;
        this.firstname = firstname;
        this.surname = surname;
        this.sex = sex;
        this.age = age;
        this.description = description;
        this.homepage = homepage;
        this.creationTime = creationTime;
        this.groupMemberEntities = groupMemberEntities;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id", unique = true, nullable = false)
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Column(name = "username", unique = true, nullable = false)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "password", unique = false, nullable = false)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "enabled", unique = false, nullable = false)
    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Column(name = "email", unique = true, nullable = false)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "firstname", unique = false, nullable = true)
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @Column(name = "surname", unique = false, nullable = true)
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Column(name = "sex", unique = false, nullable = true, length = 1)
    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Column(name = "age", unique = false, nullable = true)
    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Column(name = "description", unique = false, nullable = true)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "homepage", unique = false, nullable = true)
    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    @Column(name = "creation_time", unique = false, nullable = true)
    public Timestamp getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Timestamp creationTime) {
        this.creationTime = creationTime;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userEntity", targetEntity = GroupMemberEntity.class, cascade = {CascadeType.MERGE, CascadeType.PERSIST}, orphanRemoval = true)
    public Set<GroupMemberEntity> getGroupMemberEntities() {
        return groupMemberEntities;
    }

    public void setGroupMemberEntities(Set<GroupMemberEntity> groupMemberEntities) {
        this.groupMemberEntities = groupMemberEntities;
    }
}
