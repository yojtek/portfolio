package pl.mydomain.portfolio.domain.entity;

import javax.persistence.*;

@Entity
@Table(name = "group_members")
public class GroupMemberEntity {

    private Long groupMemberId;
    private GroupEntity groupEntity;
    private UserEntity userEntity;

    public GroupMemberEntity() {
    }

    public GroupMemberEntity(Long groupMemberId, GroupEntity groupEntity, UserEntity userEntity) {
        this.groupMemberId = groupMemberId;
        this.groupEntity = groupEntity;
        this.userEntity = userEntity;
    }

    public GroupMemberEntity(GroupEntity groupEntity, UserEntity userEntity) {
        this.groupEntity = groupEntity;
        this.userEntity = userEntity;
    }


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "group_member_id", unique = true, nullable = false)
    public Long getGroupMemberId() {
        return groupMemberId;
    }

    public void setGroupMemberId(Long groupMemberId) {
        this.groupMemberId = groupMemberId;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST}
    )
    @JoinColumn(name = "group_id")
    public GroupEntity getGroupEntity() {
        return groupEntity;
    }

    public void setGroupEntity(GroupEntity groupEntity) {
        this.groupEntity = groupEntity;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "user_id")
    public UserEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }
}
