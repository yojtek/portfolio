package pl.mydomain.portfolio.domain.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name = "groups")
public class GroupEntity {

    private Long groupId;
    private String groupName;
    private Set<GroupAuthorityEntity> groupAuthorityEntities = new HashSet<>();
    private Set<GroupMemberEntity> groupMemberEntities = new HashSet<>();

    public GroupEntity() {
    }

    public GroupEntity(String group_name) {
        this.groupName = group_name;
    }

    public GroupEntity(Long groupId, String groupName, Set<GroupAuthorityEntity> groupAuthorityEntities,
                       Set<GroupMemberEntity> groupMemberEntities) {
        this.groupId = groupId;
        this.groupName = groupName;
        this.groupAuthorityEntities = groupAuthorityEntities;
        this.groupMemberEntities = groupMemberEntities;
    }

    public GroupEntity(String groupName, Set<GroupAuthorityEntity> groupAuthorityEntities,
                       Set<GroupMemberEntity> groupMemberEntities) {
        this.groupName = groupName;
        this.groupAuthorityEntities = groupAuthorityEntities;
        this.groupMemberEntities = groupMemberEntities;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "group_id", unique = true, nullable = false)
    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    @Column(name = "group_name", unique = true, nullable = false)
    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "groupEntity", targetEntity = GroupAuthorityEntity.class, cascade = {CascadeType.MERGE, CascadeType.PERSIST}, orphanRemoval = true)
    public Set<GroupAuthorityEntity> getGroupAuthorityEntities() {
        return groupAuthorityEntities;
    }

    public void setGroupAuthorityEntities(Set<GroupAuthorityEntity> groupAuthorityEntities) {
        this.groupAuthorityEntities = groupAuthorityEntities;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "groupEntity", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, orphanRemoval = true)
    public Set<GroupMemberEntity> getGroupMemberEntities() {
        return groupMemberEntities;
    }

    public void setGroupMemberEntities(Set<GroupMemberEntity> groupMemberEntities) {
        this.groupMemberEntities = groupMemberEntities;
    }
}