package pl.mydomain.portfolio.domain.entity;

import javax.persistence.*;

@Entity
@Table(name = "group_authorities")
public class GroupAuthorityEntity {

    private Long groupAuthorityId;
    private GroupEntity groupEntity;
    private AuthorityEntity authorityEntity;

    public GroupAuthorityEntity() {
    }

    public GroupAuthorityEntity(AuthorityEntity authorityEntity, GroupEntity groupEntity) {
        this.groupEntity = groupEntity;
        this.authorityEntity = authorityEntity;
    }

    public GroupAuthorityEntity(Long groupAuthorityId, AuthorityEntity authorityEntity, GroupEntity groupEntity) {
        this.groupAuthorityId = groupAuthorityId;
        this.groupEntity = groupEntity;
        this.authorityEntity = authorityEntity;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "group_authority_id", unique = true, nullable = false)
    public Long getGroupAuthorityId() {
        return groupAuthorityId;
    }

    public void setGroupAuthorityId(Long groupAuthorityId) {
        this.groupAuthorityId = groupAuthorityId;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "group_id", unique = false, nullable = false)
    public GroupEntity getGroupEntity() {
        return groupEntity;
    }

    public void setGroupEntity(GroupEntity groupEntity) {
        this.groupEntity = groupEntity;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "authority_id", unique = false, nullable = false)
    public AuthorityEntity getAuthorityEntity() {
        return authorityEntity;
    }

    public void setAuthorityEntity(AuthorityEntity authorityEntity) {
        this.authorityEntity = authorityEntity;
    }
}
