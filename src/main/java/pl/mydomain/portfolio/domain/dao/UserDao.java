package pl.mydomain.portfolio.domain.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.mydomain.portfolio.domain.entity.UserEntity;

import java.util.List;

@Repository("UserDao")
public interface UserDao extends JpaRepository<UserEntity, Long> {

    UserEntity findByUsername(String username);

    UserEntity findByEmail(String email);

    @Query(nativeQuery = true, value = "SELECT user_id, username, password, enabled, email, firstname, surname, " +
            "       sex, age, description, homepage, creation_time " +
            "  FROM users " +
            "  ORDER BY " +
            "  CASE WHEN ?1 = 'orderByUsernameASC' THEN username END ASC " +
            ", CASE WHEN ?1 = 'orderByUsernameDESC' THEN username END DESC " +
            ", CASE WHEN ?1 = 'orderByFirstnameASC' THEN firstname END ASC " +
            ", CASE WHEN ?1 = 'orderByFirstnameDESC' THEN firstname END DESC " +
            ", CASE WHEN ?1 = 'orderBySurnameASC' THEN surname END ASC " +
            ", CASE WHEN ?1 = 'orderBySurnameDESC' THEN surname END DESC " +
            ", CASE WHEN ?1 = 'orderByEmailASC' THEN email END ASC " +
            ", CASE WHEN ?1 = 'orderByEmailDESC' THEN email END DESC " +
            ", CASE WHEN ?1 = 'orderByCreationTimeASC' THEN creation_time END ASC " +
            "   , CASE WHEN ?1 = 'orderByCreationTimeDESC' THEN creation_time END DESC " +
            ", CASE WHEN ?1  = 'orderByAgeASC' THEN age END ASC " +
            ", CASE WHEN ?1 = 'orderByAgeDESC' THEN age END DESC " +
            "   LIMIT ?3  OFFSET ?2 ; ")
    List<UserEntity> getAll(String orderPattern, int offset, int limit);
}
