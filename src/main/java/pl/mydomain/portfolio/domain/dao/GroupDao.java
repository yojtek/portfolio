package pl.mydomain.portfolio.domain.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.mydomain.portfolio.domain.entity.GroupEntity;

@Repository("GroupDao")
public interface GroupDao extends JpaRepository<GroupEntity, Long> {

    GroupEntity findByGroupName(String groupName);
}
