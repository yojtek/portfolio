package pl.mydomain.portfolio.domain.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.mydomain.portfolio.domain.entity.GroupMemberEntity;

@Repository("GroupMemberDao")
public interface GroupMemberDao extends JpaRepository<GroupMemberEntity, Long> {
}
