package pl.mydomain.portfolio.domain.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.mydomain.portfolio.domain.entity.AuthorityEntity;

@Repository("AuthorityDao")
public interface AuthorityDao extends JpaRepository<AuthorityEntity, Long> {
}
