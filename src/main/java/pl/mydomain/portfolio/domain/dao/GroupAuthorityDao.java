package pl.mydomain.portfolio.domain.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.mydomain.portfolio.domain.entity.GroupAuthorityEntity;

@Repository("GroupAuthorityDao")
public interface GroupAuthorityDao extends JpaRepository<GroupAuthorityEntity, Long> {
}
