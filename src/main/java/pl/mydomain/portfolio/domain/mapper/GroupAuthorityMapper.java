package pl.mydomain.portfolio.domain.mapper;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.mydomain.portfolio.domain.entity.*;
import pl.mydomain.portfolio.domain.pojo.AuthorityJson;
import pl.mydomain.portfolio.domain.pojo.GroupJson;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GroupAuthorityMapper {

    protected Logger logger = LoggerFactory.getLogger(GroupAuthorityMapper.class);
    private UserMapper userMapper = new UserMapper();

    @Deprecated
    public List<GroupJson> convertGroupEntityListToJsonList(Set<GroupEntity> groupEntitySet) {
        List<GroupJson> groupJsonList = new ArrayList<>();
        if (groupEntitySet != null && !groupEntitySet.isEmpty()) {
            for (GroupEntity groupEntity : groupEntitySet) {
                groupJsonList.add(convertGroupEntityToJson(groupEntity));
            }
        } else {
            logger.warn("Trying convert groupMemberEntitySet to jsonList, but groupMemberEntitySet is NULL or EMPTY !!");
        }
        return groupJsonList;
    }

    public List<GroupJson> convertGroupEntityListToJsonList(List<GroupEntity> groupEntityList) {
        List<GroupJson> groupJsonList = new ArrayList<>();
        if (groupEntityList != null && !groupEntityList.isEmpty()) {
            for (GroupEntity groupEntity : groupEntityList) {
                groupJsonList.add(convertGroupEntityToJson(groupEntity));
            }
        } else {
            logger.warn("Trying convert groupMemberEntitySet to jsonList, but groupMemberEntitySet is NULL or EMPTY !!");
        }
        return groupJsonList;
    }

    public GroupJson convertGroupEntityToJson(GroupEntity groupEntity) {
        if (groupEntity != null) {
            if (groupEntity.getGroupAuthorityEntities() != null && !groupEntity.getGroupAuthorityEntities().isEmpty()) {
                Set<AuthorityEntity> authorityEntitySet = new HashSet<>();
                for (GroupAuthorityEntity groupAuthorityEntity : groupEntity.getGroupAuthorityEntities()) {
                    authorityEntitySet.add(groupAuthorityEntity.getAuthorityEntity());
                }
                if (!groupEntity.getGroupMemberEntities().isEmpty()) {
                    List<UserEntity> userEntityList = new ArrayList<>();
                    for (GroupMemberEntity groupMemberEntity : groupEntity.getGroupMemberEntities()) {
                        userEntityList.add(groupMemberEntity.getUserEntity());
                    }
                    return new GroupJson(groupEntity.getGroupId(), groupEntity.getGroupName(), convertAuthorityEntityListToJsonList(authorityEntitySet), userMapper.convertUserEntityListToJsonList(userEntityList));
                }
                return new GroupJson(groupEntity.getGroupId(), groupEntity.getGroupName(), convertAuthorityEntityListToJsonList(authorityEntitySet), null);
            }
            return new GroupJson(groupEntity.getGroupId(), groupEntity.getGroupName(), null, null);
        }
        logger.warn("Trying convert groupEntity to json, but groupEntity is NULL !!");
        return new GroupJson();
    }

    public List<AuthorityJson> convertAuthorityEntityListToJsonList(Set<AuthorityEntity> authorityEntitySet) {
        List<AuthorityJson> authorityJsonList = new ArrayList<>();
        if (authorityEntitySet != null && !authorityEntitySet.isEmpty()) {
            for (AuthorityEntity authorityEntity : authorityEntitySet) {
                authorityJsonList.add(convertAuthorityEntityToJson(authorityEntity));
            }
        } else {
            logger.warn("Trying convert groupMemberEntitySet to jsonList, but groupMemberEntitySet is NULL or EMPTY !!");
        }
        return authorityJsonList;
    }

    public AuthorityJson convertAuthorityEntityToJson(AuthorityEntity authorityEntity) {
        if (authorityEntity != null) {
            return new AuthorityJson(authorityEntity.getAuthorityId(), authorityEntity.getAuthorityName());
        }
        logger.warn("Trying convert authorityEntity to json, but authorityEntity is NULL !!");
        return new AuthorityJson();
    }

    public List<AuthorityJson> convertAuthorityEntityListToJsonList(List<AuthorityEntity> authorityEntityList) {
        List<AuthorityJson> authorityJsonList = new ArrayList<>();
        if (authorityEntityList != null && !authorityEntityList.isEmpty()) {
            for (AuthorityEntity authorityEntity : authorityEntityList) {
                authorityJsonList.add(convertAuthorityEntityToJson(authorityEntity));
            }
        } else {
            logger.warn("Trying convert groupMemberEntitySet to jsonList, but groupMemberEntitySet is NULL or EMPTY !!");
        }
        return authorityJsonList;
    }

}
