package pl.mydomain.portfolio.domain.mapper;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.mydomain.portfolio.domain.entity.UserEntity;
import pl.mydomain.portfolio.domain.pojo.UserJson;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UserMapper {

    protected Logger logger = LoggerFactory.getLogger(UserMapper.class);

    public List<UserJson> convertUserEntityListToJsonList(List<UserEntity> userEntityList) {
        List<UserJson> userJsonList = new ArrayList<>();
        if (userEntityList != null && !userEntityList.isEmpty()) {
            userJsonList
                    .addAll(userEntityList
                            .stream()
                            .map(this::convertUserEntityToJson).collect(Collectors.toList()));
        } else {
            logger.warn("Trying convert userEntityList to jsonList, but userEntityList is NULL or EMPTY !!");
        }
        return userJsonList;
    }

    public UserJson convertUserEntityToJson(UserEntity userEntity) {
        if (userEntity != null) {
            return new UserJson(userEntity.getUserId(),
                    userEntity.getUsername(),
                    userEntity.getPassword(),
                    userEntity.getEnabled(),
                    userEntity.getEmail(),
                    userEntity.getFirstname(),
                    userEntity.getSurname(),
                    userEntity.getSex(),
                    userEntity.getAge(),
                    userEntity.getDescription(),
                    userEntity.getHomepage(),
                    userEntity.getCreationTime(),
                    null);
        }
        logger.warn("Trying convert userEntity to json, but userEntity is NULL !!");
        return new UserJson();
    }
}
