package pl.mydomain.portfolio.domain.pojo;

import java.util.ArrayList;
import java.util.List;

public class AuthJson {

    private String loggedUser;
    private List<String> views;

    public AuthJson() {
        views = new ArrayList<>();
    }

    public AuthJson(String loggedUser, List<String> views) {
        this.loggedUser = loggedUser;
        this.views = views;
    }

    public String getLoggedUser() {
        return loggedUser;
    }

    public void setLoggedUser(String loggedUser) {
        this.loggedUser = loggedUser;
    }

    public List<String> getViews() {
        return views;
    }

    public void setViews(List<String> views) {
        this.views = views;
    }

    public void addView(String view) {
        views.add(view);
    }
}
