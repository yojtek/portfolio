package pl.mydomain.portfolio.domain.pojo;

import java.util.ArrayList;
import java.util.List;

public class GroupJson extends ParentJson {

    private Long groupId;
    private String groupName;
    private List<AuthorityJson> authorityJsonList = new ArrayList<>();
    private List<UserJson> userJsonList = new ArrayList<>();

    public GroupJson() {
    }

    public GroupJson(Long groupId, String groupName, List<AuthorityJson> authorityJsonList, List<UserJson> userJsonList) {
        this.groupId = groupId;
        this.groupName = groupName;
        this.authorityJsonList = authorityJsonList;
        this.userJsonList = userJsonList;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<AuthorityJson> getAuthorityJsonList() {
        return authorityJsonList;
    }

    public void setAuthorityJsonList(List<AuthorityJson> authorityJsonList) {
        this.authorityJsonList = authorityJsonList;
    }

    public List<UserJson> getUserJsonList() {
        return userJsonList;
    }

    public void setUserJsonList(List<UserJson> userJsonList) {
        this.userJsonList = userJsonList;
    }
}
