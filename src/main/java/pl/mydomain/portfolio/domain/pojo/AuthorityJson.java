package pl.mydomain.portfolio.domain.pojo;


public class AuthorityJson extends ParentJson {

    private Long authorityId;
    private String authorityName;

    public AuthorityJson() {
    }

    public AuthorityJson(Long authorityId, String authorityName) {
        this.authorityId = authorityId;
        this.authorityName = authorityName;
    }

    public Long getAuthorityId() {
        return authorityId;
    }

    public void setAuthorityId(Long authorityId) {
        this.authorityId = authorityId;
    }

    public String getAuthorityName() {
        return authorityName;
    }

    public void setAuthorityName(String authorityName) {
        this.authorityName = authorityName;
    }
}
