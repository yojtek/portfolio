package pl.mydomain.portfolio.domain.pojo;


import java.util.List;

public class JsonListCover {

    private List<ParentJson> parentJsonList;
    private String requestMessage;
    private String responseMessage;
    private Boolean responseResult;

    public JsonListCover() {
    }

    public JsonListCover(List<ParentJson> parentJsonList, String requestMessage,
                         String responseMessage, Boolean responseResult) {
        this.parentJsonList = parentJsonList;
        this.requestMessage = requestMessage;
        this.responseMessage = responseMessage;
        this.responseResult = responseResult;
    }

    public List<ParentJson> getParentJsonList() {
        return parentJsonList;
    }

    public void setParentJsonList(List<ParentJson> parentJsonList) {
        this.parentJsonList = parentJsonList;
    }

    public String getRequestMessage() {
        return requestMessage;
    }

    public void setRequestMessage(String requestMessage) {
        this.requestMessage = requestMessage;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public Boolean getResponseResult() {
        return responseResult;
    }

    public void setResponseResult(Boolean responseResult) {
        this.responseResult = responseResult;
    }
}
