package pl.mydomain.portfolio.domain.pojo;


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class UserJson extends ParentJson {

    private Long userId;
    private String username;
    private String password;
    private Boolean enabled;
    private String email;
    private String firstname;
    private String surname;
    private String sex;
    private Integer age;
    private String description;
    private String homepage;
    private Timestamp creationTime;
    private List<GroupJson> groupJsonList = new ArrayList<>();

    public UserJson() {
    }

    public UserJson(Long userId, String username, String password, Boolean enabled,
                    String email, String firstname, String surname, String sex,
                    Integer age, String description, String homepage,
                    Timestamp creationTime, List<GroupJson> groupJsonList) {
        this.userId = userId;
        this.username = username;
        this.password = password;
        this.enabled = enabled;
        this.email = email;
        this.firstname = firstname;
        this.surname = surname;
        this.sex = sex;
        this.age = age;
        this.description = description;
        this.homepage = homepage;
        this.creationTime = creationTime;
        this.groupJsonList = groupJsonList;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public Timestamp getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Timestamp creationTime) {
        this.creationTime = creationTime;
    }

    public List<GroupJson> getGroupJsonList() {
        return groupJsonList;
    }

    public void setGroupJsonList(List<GroupJson> groupJsonList) {
        this.groupJsonList = groupJsonList;
    }
}
