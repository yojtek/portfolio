package pl.mydomain.portfolio.domain.pojo;

public class JsonCover {

    private ParentJson parentJson;
    private String requestMessage;
    private String responseMessage;
    private Boolean responseResult;


    public JsonCover() {
        responseResult = false;
        responseMessage = "";
    }

    public JsonCover(ParentJson parentJson, String requestMesage,
                     String responseMessage, Boolean responseResult) {
        this.parentJson = parentJson;
        this.requestMessage = requestMesage;
        this.responseMessage = responseMessage;
        this.responseResult = responseResult;
    }

    public ParentJson getParentJson() {
        return parentJson;
    }

    public void setParentJson(ParentJson parentJson) {
        this.parentJson = parentJson;
    }

    public String getRequestMessage() {
        return requestMessage;
    }

    public void setRequestMessage(String requestMessage) {
        this.requestMessage = requestMessage;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public Boolean getResponseResult() {
        return responseResult;
    }

    public void setResponseResult(Boolean responseResult) {
        this.responseResult = responseResult;
    }
}
