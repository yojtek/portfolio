package pl.mydomain.portfolio.config.security;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class AuthenticationFailureHandlerImpl implements AuthenticationFailureHandler {

    private static final int SESSION_TIMEOUT = 10;
    private static final String AUTHORISED_USER_ROLE = "ROLE_ACCESS";
    private static final String AUTHORISED_USER_PAGE = "index.html#/view/main";
    private static final String NOT_AUTHORISED_USER_PAGE = "index.html#/view/user/login";
    protected Logger logger = LoggerFactory.getLogger(AuthenticationFailureHandlerImpl.class);
    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        if (!response.isCommitted()) {
            request.setAttribute(WebAttributes.AUTHENTICATION_EXCEPTION, exception);
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        } else {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, exception.getMessage());
        }
    }
}