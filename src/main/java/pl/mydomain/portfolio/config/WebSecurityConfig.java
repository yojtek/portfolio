package pl.mydomain.portfolio.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.web.filter.CharacterEncodingFilter;
import pl.mydomain.portfolio.config.security.AccessDeniedHandlerImpl;
import pl.mydomain.portfolio.config.security.AuthenticationFailureHandlerImpl;
import pl.mydomain.portfolio.config.security.AuthenticationSuccessHandlerImpl;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    AuthenticationSuccessHandlerImpl authenticationSuccessHandler;
    @Autowired
    AuthenticationFailureHandlerImpl authenticationFailureHandler;
    @Autowired
    DataSource dataSource;
    @Autowired
    AccessDeniedHandlerImpl accessDeniedHandler;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        JdbcUserDetailsManager jdbcUserDetailsManager = auth.jdbcAuthentication()
                .dataSource(dataSource)
                .usersByUsernameQuery("select username, password, enabled from users where username=?")
                .groupAuthoritiesByUsername("select g.group_id, g.group_name, a.authority_name from groups g " +
                        "                        JOIN group_members gm USING(group_id) " +
                        "                        JOIN group_authorities ga USING(group_id)" +
                        "                        JOIN authorities a USING (authority_id)" +
                        "                        JOIN users u USING (user_id)" +
                        "                        WHERE u.username = ?;")
                .passwordEncoder(passwordEncoder())
                .getUserDetailsService();
        jdbcUserDetailsManager.setEnableAuthorities(false);
        jdbcUserDetailsManager.setEnableGroups(true);
    }

    @Override
    public void configure(HttpSecurity httpSecurity) throws Exception {
        CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
        characterEncodingFilter.setEncoding("UTF-8");
        characterEncodingFilter.setForceEncoding(true);
        httpSecurity.addFilterBefore(characterEncodingFilter, CsrfFilter.class);
        httpSecurity
                .authorizeRequests()
                .antMatchers("/index.html", "/index.html#/**").permitAll()
                .antMatchers("/lib/**", "/img/**", "/scripts/**", "/views/**").permitAll()
                .antMatchers("/api/auth/user").permitAll()
                .antMatchers("/api/group", "/api/gorup/**").hasRole("API_GROUP")
                .antMatchers("/api/user", "/api/user/**").hasRole("API_USER")
                .anyRequest().denyAll()
                .and().exceptionHandling().accessDeniedHandler(accessDeniedHandler)
                .and()
                .formLogin().loginPage("/index.html#/view/main")
                .loginProcessingUrl("/process")
                .successHandler(authenticationSuccessHandler)
                .failureHandler(authenticationFailureHandler)
                .and().logout().logoutUrl("/logout").and().csrf().disable();
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
