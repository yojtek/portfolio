package pl.mydomain.portfolio.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.PathResource;

import java.nio.file.Path;
import java.nio.file.Paths;

@Configuration
@ComponentScan("pl.mydomain.portfolio.*")
@Import({ResourceConfig.class, ResourceConfig.class, WebSecurityConfig.class})
@EnableAutoConfiguration
public class AppConfig {

    private static final Path LEGACY_CONF_PATH = Paths.get(System.getenv("CATALINA_HOME"), "conf", "configPortfolio.properties");

    @Bean
    public static PropertySourcesPlaceholderConfigurer getSourcesPropertyPlaceholderConfigurer() {
        PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer();

        configurer.setLocation(new PathResource(LEGACY_CONF_PATH));

        return configurer;
    }
}
