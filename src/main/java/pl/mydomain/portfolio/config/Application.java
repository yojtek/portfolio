package pl.mydomain.portfolio.config;


import org.springframework.boot.SpringApplication;

import java.io.IOException;

public class Application {

    public static void main(String[] args) throws IOException {
        SpringApplication.run(AppConfig.class, args);
    }
}
