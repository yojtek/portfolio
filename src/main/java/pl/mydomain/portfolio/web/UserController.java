package pl.mydomain.portfolio.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.mydomain.portfolio.domain.pojo.JsonCover;
import pl.mydomain.portfolio.domain.pojo.UserJson;
import pl.mydomain.portfolio.service.UserService;

import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/{orderpattern}/{currentpage}/{pagesize}", method = RequestMethod.GET)
    @ResponseBody
    public List<UserJson> getAllUsers(@PathVariable("orderpattern") String orderPattern,
                                      @PathVariable("currentpage") int currentPageNumber,
                                      @PathVariable("pagesize") int pageSize) {
        return userService.getAllUsers(orderPattern, currentPageNumber, pageSize);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public UserJson getUserJson(@PathVariable("id") Long id) {
        return userService.getUser(id);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public JsonCover createUser(@RequestBody UserJson userJson) {
        return userService.createUser(userJson);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    @ResponseBody
    public JsonCover updateUser(@RequestBody UserJson userJson, @PathVariable Long id) {
        return userService.updateUser(id, userJson);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public JsonCover deleteUser(@PathVariable("id") Long id) {
        return userService.deleteUser(id);
    }

}
