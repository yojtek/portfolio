package pl.mydomain.portfolio.web;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import pl.mydomain.portfolio.domain.pojo.AuthJson;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    @ResponseBody
    public AuthJson getAuthJson(HttpServletRequest request) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        AuthJson authJson = new AuthJson();
        authJson.setLoggedUser(auth.getName());
        for (GrantedAuthority grantedAuthority : auth.getAuthorities()) {
            if (grantedAuthority.getAuthority().contains("VIEW")) {
                String view = grantedAuthority.getAuthority();
                String[] fixedView = view.split("ROLE_VIEW_");
                authJson.addView(fixedView[1].toLowerCase());
            }
        }
        return authJson;
    }
}
