package pl.mydomain.portfolio.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.mydomain.portfolio.domain.pojo.AuthorityJson;
import pl.mydomain.portfolio.domain.pojo.GroupJson;
import pl.mydomain.portfolio.domain.pojo.JsonCover;
import pl.mydomain.portfolio.service.GroupAuthorityService;

import java.util.List;

@RestController
@RequestMapping("/api/group")
public class GroupAuthorityController {

    @Autowired
    GroupAuthorityService groupAuthorityService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    public List<GroupJson> getAllGroupJson() {
        return groupAuthorityService.getAllGroups();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public GroupJson getGroupJson(@PathVariable("id") Long id) {
        return groupAuthorityService.getGroupById(id);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public JsonCover createGroup(@RequestBody GroupJson groupJson) {
        return groupAuthorityService.createGroup(groupJson);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    @ResponseBody
    public JsonCover updateGroup(@RequestBody GroupJson groupJson, @PathVariable Long id) {
        return groupAuthorityService.updateGroup(id, groupJson);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public JsonCover deleteGroup(@PathVariable("id") Long id) {
        return groupAuthorityService.deleteGroupById(id);
    }

    @RequestMapping(value = "/authorities", method = RequestMethod.GET)
    @ResponseBody
    public List<AuthorityJson> getAllAuthoritiyJson() {
        return groupAuthorityService.getAllAuthorities();
    }


}
