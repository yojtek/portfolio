package pl.mydomain.portfolio.dao;

import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pl.mydomain.portfolio.config.AppConfig;
import pl.mydomain.portfolio.domain.dao.GroupDao;
import pl.mydomain.portfolio.domain.entity.GroupEntity;

/**
 * tests only custom methods
 */
@ContextConfiguration(classes = {AppConfig.class})
@RunWith(SpringJUnit4ClassRunner.class)
public class GroupAuthorityDaoTests {

    @Autowired
    GroupDao groupDao;

    GroupEntity groupEntity1;
    GroupEntity groupEntity2;

    @Before
    public void setUp() {
        groupEntity1 = new GroupEntity("junitTest", null, null);
        groupEntity2 = new GroupEntity("junitTest2", null, null);
        groupDao.save(groupEntity1);
        groupDao.save(groupEntity2);
    }

    @Test
    public void findByGroupNameTest() {
        GroupEntity groupEntityResult = groupDao.findByGroupName("junitTest2");
        TestCase.assertNotNull(groupEntityResult);
        TestCase.assertEquals(groupEntity2.getGroupName(), groupEntityResult.getGroupName());
    }

    @After
    public void clear() {
        groupDao.delete(groupEntity1);
        groupDao.delete(groupEntity2);
    }
}
