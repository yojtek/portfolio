package pl.mydomain.portfolio.dao;


import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pl.mydomain.portfolio.config.AppConfig;
import pl.mydomain.portfolio.domain.dao.UserDao;
import pl.mydomain.portfolio.domain.entity.UserEntity;
import pl.mydomain.portfolio.util.DateUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * test only custom methods
 */
@ContextConfiguration(classes = {AppConfig.class})
@RunWith(SpringJUnit4ClassRunner.class)
public class UserDaoTest {

    @Autowired
    UserDao userDao;

    UserEntity userEntityTest1;
    UserEntity userEntityTest2;
    UserEntity userEntityTest3;
    List<UserEntity> userEntityList;

    @Before
    public void setup() {
        userEntityTest1 = new UserEntity("junitLogin1", "Password1", true,
                "email1@gmail.com", "Firstname", "Surname", "m", 20, "jUnit test",
                "www.homepage1.com", DateUtil.getCurrentTimestamp(), null);
        userEntityTest2 = new UserEntity("junitLogin2", "Password2", true,
                "email2@gmail.com", "Firstname1", "Surname1", "m", 30, "jUnit test",
                "www.homepage2.com", DateUtil.getCurrentTimestamp(), null);
        userEntityTest3 = new UserEntity("junitLogin3", "Password3", true,
                "email3@gmail.com", "Firstname3", "Surname3", "m", 40, "jUnit test",
                "www.homepage3.com", DateUtil.getCurrentTimestamp(), null);
        userEntityList = new ArrayList<>();
        userEntityList.add(userEntityTest1);
        userEntityList.add(userEntityTest2);
        userEntityList.add(userEntityTest3);
        userDao.delete(userEntityList);
        userDao.save(userEntityList);
    }

    @Test
    public void findByUsernameTest() {
        UserEntity userEntityResult = userDao.findByUsername("junitLogin3");
        TestCase.assertNotNull(userEntityResult);
        TestCase.assertEquals(userEntityTest3.getUsername(), userEntityResult.getUsername());
    }

    @Test
    public void findByEmailTest() {
        UserEntity userEntityResult = userDao.findByEmail("email2@gmail.com");
        TestCase.assertNotNull(userEntityResult);
        TestCase.assertEquals(userEntityTest2.getUsername(), userEntityResult.getUsername());

    }

    @Test
    @Ignore //TODO: fix it
    public void getAllTest() {
        List<UserEntity> userEntityListResult = userDao.getAll("orderByAgeASC", 0, 5);
        TestCase.assertFalse(userEntityListResult.isEmpty());
        TestCase.assertTrue(userEntityListResult.size() <= 5);
        for (int i = 0; i < 4; i++) {
            if (userEntityListResult.get(i).getAge() != null && userEntityListResult.get(i + 1).getAge() != null)
                TestCase.assertTrue(userEntityListResult.get(i).getAge() <= userEntityListResult.get(i + 1).getAge());
        }

        userEntityListResult = userDao.getAll("orderByAgeDESC", 0, 5);
        TestCase.assertFalse(userEntityListResult.isEmpty());
        for (int i = 0; i < 4; i++) {
            if (userEntityListResult.get(i).getAge() != null && userEntityListResult.get(i + 1).getAge() != null)
                TestCase.assertTrue(userEntityListResult.get(i).getAge() >= userEntityListResult.get(i + 1).getAge());
        }
    }

    @After
    public void clear() {
        userDao.delete(userEntityList);
    }
}
