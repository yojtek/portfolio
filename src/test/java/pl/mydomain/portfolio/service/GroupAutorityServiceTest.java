package pl.mydomain.portfolio.service;


import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.powermock.reflect.Whitebox;
import pl.mydomain.portfolio.domain.dao.AuthorityDao;
import pl.mydomain.portfolio.domain.dao.GroupAuthorityDao;
import pl.mydomain.portfolio.domain.dao.GroupDao;
import pl.mydomain.portfolio.domain.entity.*;
import pl.mydomain.portfolio.domain.pojo.AuthorityJson;
import pl.mydomain.portfolio.domain.pojo.GroupJson;
import pl.mydomain.portfolio.domain.pojo.JsonCover;
import pl.mydomain.portfolio.util.DateUtil;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GroupAutorityServiceTest {

    GroupAuthorityService groupAuthorityService;
    //  UserDao userDaoMocked;
  /*  GroupMemberDao groupMemberDaoMocked;*/
    GroupDao groupDaoMocked;
    GroupAuthorityDao groupAuthorityDaoMocked;
    AuthorityDao authorityDaoMocked;

    UserEntity userEntityTest1;
    UserEntity userEntityTest2;
    UserEntity userEntityTest3;

    AuthorityEntity authorityEntityTest1;
    AuthorityEntity authorityEntityTest2;
    AuthorityEntity authorityEntityTest3;

    GroupEntity groupEntityTest1;
    GroupEntity groupEntityTest2;
    GroupEntity groupEntityTest3;

    GroupAuthorityEntity groupAuthorityEntityTest1;
    GroupAuthorityEntity groupAuthorityEntityTest2;

    GroupMemberEntity groupMemberEntityTest1;

    Set<GroupMemberEntity> groupMemberEntitySetTest1 = new HashSet<>();
    Set<GroupAuthorityEntity> groupAuthorityEntitySetTest1 = new HashSet<>();

    @Before
    public void setUp() throws Exception {

        groupAuthorityService = new GroupAuthorityService();
        //  userDaoMocked = mock(UserDao.class);
       /* groupMemberDaoMocked = mock(GroupMemberDao.class);*/
        groupDaoMocked = mock(GroupDao.class);
        groupAuthorityDaoMocked = mock(GroupAuthorityDao.class);
        authorityDaoMocked = mock(AuthorityDao.class);

        /*Whitebox.setInternalState(groupAuthorityService, "userDao", userDaoMocked);
        Whitebox.setInternalState(groupAuthorityService, "groupMemberDao", groupMemberDaoMocked);*/
        Whitebox.setInternalState(groupAuthorityService, "groupDao", groupDaoMocked);
        Whitebox.setInternalState(groupAuthorityService, "groupAuthorityDao", groupAuthorityDaoMocked);
        Whitebox.setInternalState(groupAuthorityService, "authorityDao", authorityDaoMocked);

        userEntityTest1 = new UserEntity(1L, "Login1", "Password1", true,
                "email1@gmail.com,", "Firstname", "Surname", "m", 20, "description1",
                "www.homepage1.com", DateUtil.getCurrentTimestamp(), groupMemberEntitySetTest1);
        userEntityTest2 = new UserEntity(2L, "Login2", "Password2", true,
                "email1@gmail.com,", "Firstname1", "Surname1", "m", 30, "description2",
                "www.homepage2.com", DateUtil.getCurrentTimestamp(), null);
        userEntityTest3 = new UserEntity(3L, "Login3", "Password3", true,
                "email@gmail3.com,", "Firstname3", "Surname3", "m", 40, "description3",
                "www.homepage3.com", DateUtil.getCurrentTimestamp(), null);

        authorityEntityTest1 = new AuthorityEntity(1L, "ROLE_TEST1", groupAuthorityEntitySetTest1);
        authorityEntityTest2 = new AuthorityEntity(2L, "ROLE_TEST2", groupAuthorityEntitySetTest1);
        authorityEntityTest3 = new AuthorityEntity(3L, "ROLE_TEST2", null);

        groupEntityTest1 = new GroupEntity(1L, "test_group1", groupAuthorityEntitySetTest1, groupMemberEntitySetTest1);
        groupEntityTest2 = new GroupEntity(2L, "test_group2", null, null);
        groupEntityTest3 = new GroupEntity(3L, "test_group3", null, null);

        groupAuthorityEntityTest1 = new GroupAuthorityEntity(1L, authorityEntityTest1, groupEntityTest1);
        groupAuthorityEntityTest2 = new GroupAuthorityEntity(2L, authorityEntityTest2, groupEntityTest1);

        groupMemberEntityTest1 = new GroupMemberEntity(1L, groupEntityTest1, userEntityTest1);

        groupMemberEntitySetTest1.add(groupMemberEntityTest1);
        groupAuthorityEntitySetTest1.add(groupAuthorityEntityTest1);
        groupAuthorityEntitySetTest1.add(groupAuthorityEntityTest2);

    }

    @Test
    public void getGroupsAndAuthoritiesForUserTest() {
        List<GroupJson> groupJsonList = groupAuthorityService.getGroupsAndAuthoritiesForUser(userEntityTest1);
        TestCase.assertNotNull(groupJsonList);
        TestCase.assertTrue(groupJsonList.size() == 1);
        TestCase.assertEquals(groupJsonList.get(0).getGroupId(), groupEntityTest1.getGroupId());
        TestCase.assertEquals(groupJsonList.get(0).getGroupName(), groupEntityTest1.getGroupName());
        TestCase.assertTrue(groupJsonList.get(0).getAuthorityJsonList().size() == 2);
        for (AuthorityJson authorityJson : groupJsonList.get(0).getAuthorityJsonList()) {
            if (authorityJson.getAuthorityId() == authorityEntityTest1.getAuthorityId()) {
                TestCase.assertEquals(authorityEntityTest1.getAuthorityName(), authorityJson.getAuthorityName());
            } else if (authorityJson.getAuthorityId() == authorityEntityTest2.getAuthorityId()) {
                TestCase.assertEquals(authorityEntityTest2.getAuthorityName(), authorityJson.getAuthorityName());
            } else {
                TestCase.assertTrue("Authority not found in the list", false);
            }
        }
    }

    @Test
    public void createGroupTest() {

        when(groupDaoMocked.findByGroupName("TestGroup")).thenReturn(null);
        GroupJson groupJsonTest = new GroupJson(1L, "TestGroup", null, null);
        AuthorityJson authorityJson1 = new AuthorityJson(1L, "TestAuthority1");
        AuthorityJson authorityJson2 = new AuthorityJson(2L, "TestAuthority1");
        List<AuthorityJson> authorityJsonList = new ArrayList<>();
        authorityJsonList.add(authorityJson1);
        authorityJsonList.add(authorityJson2);
        groupJsonTest.setAuthorityJsonList(authorityJsonList);
        JsonCover jsonCover;
        jsonCover = groupAuthorityService.createGroup(groupJsonTest);
        TestCase.assertNotNull(jsonCover);
        TestCase.assertTrue(jsonCover.getResponseResult());

        when(groupDaoMocked.findByGroupName("TestGroup")).thenReturn(new GroupEntity());
        when(authorityDaoMocked.findOne(1L)).thenReturn(new AuthorityEntity());
        when(authorityDaoMocked.findOne(2L)).thenReturn(new AuthorityEntity());
        jsonCover = groupAuthorityService.createGroup(groupJsonTest);
        TestCase.assertNotNull(jsonCover);
        TestCase.assertFalse(jsonCover.getResponseResult());


    }

    @Test
    public void updateGroupByIdTest() {
        GroupEntity groupEntity = new GroupEntity();
        groupEntity.setGroupId(1L);
        groupEntity.setGroupName("TestGroup");
        when(groupDaoMocked.findOne(1L)).thenReturn(groupEntity);
        when(groupDaoMocked.findByGroupName("TestGroup")).thenReturn(groupEntity);
        when(authorityDaoMocked.findOne(1L)).thenReturn(new AuthorityEntity());
        when(authorityDaoMocked.findOne(2L)).thenReturn(new AuthorityEntity());
        GroupJson groupJsonTest = new GroupJson(1L, "TestGroup", null, null);
        AuthorityJson authorityJson1 = new AuthorityJson(1L, "TestAuthority1");
        AuthorityJson authorityJson2 = new AuthorityJson(2L, "TestAuthority1");
        List<AuthorityJson> authorityJsonList = new ArrayList<>();
        authorityJsonList.add(authorityJson1);
        authorityJsonList.add(authorityJson2);
        groupJsonTest.setAuthorityJsonList(authorityJsonList);
        JsonCover jsonCover;

        jsonCover = groupAuthorityService.updateGroup(1L, groupJsonTest);
        TestCase.assertNotNull(jsonCover);
        TestCase.assertTrue(jsonCover.getResponseResult());
    }

    @Test
    public void deleteGroupByIdTest() {
        GroupEntity groupEntity = new GroupEntity(1L, "TestGroup", null, null);
        when(groupDaoMocked.findOne(1L)).thenReturn(groupEntity);
        GroupJson groupJsonTest = new GroupJson(1L, "TestGroup", null, null);
        AuthorityJson authorityJson1 = new AuthorityJson(1L, "TestAuthority1");
        AuthorityJson authorityJson2 = new AuthorityJson(2L, "TestAuthority1");
        List<AuthorityJson> authorityJsonList = new ArrayList<>();
        authorityJsonList.add(authorityJson1);
        authorityJsonList.add(authorityJson2);
        groupJsonTest.setAuthorityJsonList(authorityJsonList);
        JsonCover jsonCover;

        jsonCover = groupAuthorityService.deleteGroupById(groupJsonTest.getGroupId());
        TestCase.assertNotNull(jsonCover);
        TestCase.assertTrue(jsonCover.getResponseResult());
    }

    @Test
    public void getGroupByIdTest() {
        when(groupDaoMocked.findOne(1L)).thenReturn(new GroupEntity(1L, "TestGroup", null, null));
        GroupJson groupJsonTest = new GroupJson(1L, "TestGroup", null, null);
        AuthorityJson authorityJson1 = new AuthorityJson(1L, "TestAuthority1");
        AuthorityJson authorityJson2 = new AuthorityJson(2L, "TestAuthority1");
        List<AuthorityJson> authorityJsonList = new ArrayList<>();
        groupJsonTest.setAuthorityJsonList(authorityJsonList);


        GroupJson groupJsonTestResult = groupAuthorityService.getGroupById(groupJsonTest.getGroupId());
        TestCase.assertNotNull(groupJsonTestResult);
        TestCase.assertEquals(groupJsonTest.getGroupId(), groupJsonTestResult.getGroupId());
        TestCase.assertEquals(groupJsonTest.getGroupName(), groupJsonTestResult.getGroupName());
//        TestCase.assertEquals(groupJsonTest.getAuthorityJsonList().size(), groupJsonTestResult.getAuthorityJsonList().size());

    }

    @Test
    public void getAllGroupsTest() {
// mocked methods here when ...
        AuthorityJson authorityJson1 = new AuthorityJson(1L, "TestAuthority1");
        AuthorityJson authorityJson2 = new AuthorityJson(2L, "TestAuthority1");
        List<AuthorityJson> authorityJsonList = new ArrayList<>();
        authorityJsonList.add(authorityJson1);
        authorityJsonList.add(authorityJson2);
        GroupJson groupJsonTest1 = new GroupJson(1L, "TestGroup", authorityJsonList, null);
        GroupJson groupJsonTest2 = new GroupJson(2L, "TestGroup", authorityJsonList, null);
        GroupJson groupJsonTest3 = new GroupJson(3L, "TestGroup", authorityJsonList, null);
        List<GroupJson> groupJsonList = new ArrayList<>();

        List<GroupJson> groupJsonListResult = groupAuthorityService.getAllGroups();
        TestCase.assertNotNull(groupJsonListResult);
        TestCase.assertEquals(groupJsonList.size(), groupJsonListResult.size());
        for (GroupJson groupJson : groupJsonListResult) {
            if (groupJson.getGroupId() == 1L) {
                TestCase.assertEquals(groupJsonTest1.getGroupName(), groupJson.getGroupName());
            } else if (groupJson.getGroupId() == 2L) {
                TestCase.assertEquals(groupJsonTest2.getGroupName(), groupJson.getGroupName());
            } else if (groupJson.getGroupId() == 3L) {
                TestCase.assertEquals(groupJsonTest3.getGroupName(), groupJson.getGroupName());
            } else {
                TestCase.assertTrue("group not found", false);
            }
        }
    }

}