package pl.mydomain.portfolio.service;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.powermock.reflect.Whitebox;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import pl.mydomain.portfolio.domain.dao.GroupDao;
import pl.mydomain.portfolio.domain.dao.GroupMemberDao;
import pl.mydomain.portfolio.domain.dao.UserDao;
import pl.mydomain.portfolio.domain.entity.*;
import pl.mydomain.portfolio.domain.mapper.UserMapper;
import pl.mydomain.portfolio.domain.pojo.JsonCover;
import pl.mydomain.portfolio.domain.pojo.UserJson;
import pl.mydomain.portfolio.util.DateUtil;

import java.sql.Timestamp;
import java.util.*;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

public class UserServiceTest {


    UserService userService;
    UserDao userDaoMocked;
    UserMapper userMapperMocked;
    GroupAuthorityService groupAuthorityServiceMocked;
    BCryptPasswordEncoder bCryptPasswordEncoderMocked;
    GroupMemberDao groupMemberDaoMocked;
    GroupDao groupDaoMocked;

    UserEntity userEntityTest1;
    UserEntity userEntityTest2;
    UserEntity userEntityTest3;

    AuthorityEntity authorityEntityTest1;
    AuthorityEntity authorityEntityTest2;
    AuthorityEntity authorityEntityTest3;

    GroupEntity groupEntityTest1;
    GroupEntity groupEntityTest2;
    GroupEntity groupEntityTest3;

    GroupAuthorityEntity groupAuthorityEntityTest1;
    GroupAuthorityEntity groupAuthorityEntityTest2;

    GroupMemberEntity groupMemberEntityTest1;

    Set<GroupMemberEntity> groupMemberEntitySetTest1 = new HashSet<>();
    Set<GroupAuthorityEntity> groupAuthorityEntitySetTest1 = new HashSet<>();

    @Before
    public void setUp() throws Exception {

        userService = new UserService();
        userDaoMocked = mock(UserDao.class);
        userMapperMocked = mock(UserMapper.class);
        bCryptPasswordEncoderMocked = mock(BCryptPasswordEncoder.class);
        groupAuthorityServiceMocked = mock(GroupAuthorityService.class);
        groupDaoMocked = mock(GroupDao.class);
        groupMemberDaoMocked = mock(GroupMemberDao.class);

        Whitebox.setInternalState(userService, "userDao", userDaoMocked);
        Whitebox.setInternalState(userService, "bCryptPasswordEncoder", bCryptPasswordEncoderMocked);
        Whitebox.setInternalState(userService, "userMapper", userMapperMocked);
        Whitebox.setInternalState(userService, "groupAuthorityService", groupAuthorityServiceMocked);
        Whitebox.setInternalState(userService, "groupDao", groupDaoMocked);
        Whitebox.setInternalState(userService, "groupMemberDao", groupMemberDaoMocked);
        userEntityTest1 = new UserEntity(1L, "Login1", "Password1", true,
                "email1@gmail.com,", "Firstname", "Surname", "m", 20, "description1",
                "www.homepage1.com", DateUtil.getCurrentTimestamp(), groupMemberEntitySetTest1);
        userEntityTest2 = new UserEntity(2L, "Login2", "Password2", true,
                "email1@gmail.com,", "Firstname1", "Surname1", "m", 30, "description2",
                "www.homepage2.com", DateUtil.getCurrentTimestamp(), null);
        userEntityTest3 = new UserEntity(3L, "Login3", "Password3", true,
                "email@gmail3.com,", "Firstname3", "Surname3", "m", 40, "description3",
                "www.homepage3.com", DateUtil.getCurrentTimestamp(), null);

        authorityEntityTest1 = new AuthorityEntity(1L, "ROLE_TEST1", groupAuthorityEntitySetTest1);
        authorityEntityTest2 = new AuthorityEntity(2L, "ROLE_TEST2", groupAuthorityEntitySetTest1);
        authorityEntityTest3 = new AuthorityEntity(3L, "ROLE_TEST2", null);

        groupEntityTest1 = new GroupEntity(1L, "test_group1", groupAuthorityEntitySetTest1, groupMemberEntitySetTest1);
        groupEntityTest2 = new GroupEntity(2L, "test_group2", null, null);
        groupEntityTest3 = new GroupEntity(3L, "test_group3", null, null);

        groupAuthorityEntityTest1 = new GroupAuthorityEntity(1L, authorityEntityTest1, groupEntityTest1);
        groupAuthorityEntityTest2 = new GroupAuthorityEntity(2L, authorityEntityTest2, groupEntityTest1);

        groupMemberEntityTest1 = new GroupMemberEntity(1L, groupEntityTest1, userEntityTest1);

        groupMemberEntitySetTest1.add(groupMemberEntityTest1);
        groupAuthorityEntitySetTest1.add(groupAuthorityEntityTest1);
        groupAuthorityEntitySetTest1.add(groupAuthorityEntityTest2);

    }

    @Test
    public void getUserJsonFromDbTest() {
        when(userDaoMocked.findOne(1L)).thenReturn(userEntityTest1);
        when(userDaoMocked.findOne(0L)).thenReturn(null);
        when(userMapperMocked.convertUserEntityToJson(userEntityTest1)).thenReturn(new UserJson(1L, "Login1", "Password1", true,
                "email1@gmail.com,", "Firstname", "Surname", "m", 20, "description1",
                "www.homepage1.com", DateUtil.getCurrentTimestamp(), null));
        UserJson userJson;
        userJson = userService.getUser(0L);
        TestCase.assertNull(userJson);
        userJson = userService.getUser(1L);
        TestCase.assertNotNull(userJson);
        TestCase.assertEquals("Wrong returned value: " + userJson.getUsername(),
                userJson.getUsername(), userEntityTest1.getUsername());
        TestCase.assertEquals("Wrong returned value: " + userJson.getPassword(),
                userJson.getPassword(), null);
        TestCase.assertEquals("Wrong returned value: " + userJson.getEmail(),
                userJson.getEmail(), userEntityTest1.getEmail());
        TestCase.assertEquals("Wrong returned value: " + userJson.getFirstname(),
                userJson.getFirstname(), userEntityTest1.getFirstname());
        TestCase.assertEquals("Wrong returned value: " + userJson.getSurname(),
                userJson.getSurname(), userEntityTest1.getSurname());
        TestCase.assertEquals("Wrong returned value: " + userJson.getSex(),
                userJson.getSex(), userEntityTest1.getSex());
        TestCase.assertEquals("Wrong returned value: " + userJson.getAge(),
                userJson.getAge(), userEntityTest1.getAge());
        TestCase.assertEquals("Wrong returned value: " + userJson.getDescription(),
                userJson.getDescription(), userEntityTest1.getDescription());
        TestCase.assertEquals("Wrong returned value: " + userJson.getEnabled(),
                userJson.getEnabled(), userEntityTest1.getEnabled());
        TestCase.assertEquals("Wrong returned value: " + userJson.getHomepage(),
                userJson.getHomepage(), userEntityTest1.getHomepage());
        TestCase.assertEquals("Wrong returned value: " + userJson.getUserId(),
                userJson.getUserId(), userEntityTest1.getUserId());
        TestCase.assertEquals("Wrong returned value: " + userJson.getHomepage(),
                userJson.getHomepage(), userEntityTest1.getHomepage());

        TestCase.assertNotNull("NPE GroupMemberEntityList for userJson", userJson.getGroupJsonList());

        TestCase.assertEquals("Wrong quantinity of groups for user", 1, userJson.getGroupJsonList().size());
    }

    @Test
    public void addUserJsonToDbTest() {
        when(userDaoMocked.findByUsername("testUsername")).thenReturn(null);
        when(userDaoMocked.findByEmail("test@email.co")).thenReturn(null);
        when(groupDaoMocked.findOne(anyLong())).thenReturn(new GroupEntity());
        when(bCryptPasswordEncoderMocked.encode(anyString())).thenReturn("testPassword");
        UserJson userJsonTest1 = new UserJson(1L, "testUsername", "testPassword", true, "test@email.com",
                "testFirstname", "testSurname", "m", 20, "testDesc", "www.test.com", new Timestamp(new Date().getTime()),
                null);
        JsonCover jsonCover = userService.createUser(userJsonTest1);
        TestCase.assertNotNull(jsonCover);
        TestCase.assertTrue(jsonCover.getResponseResult());

        when(userDaoMocked.findByUsername("testUsername2")).thenReturn(new UserEntity());
        // same user exist ...
        UserJson userJsonTest2 = new UserJson(1L, "testUsername2", "testPassword", true, "test@email.com",
                "testFirstname", "testSurname", "m", 20, "testDesc", "www.test.com", new Timestamp(new Date().getTime()),
                null);
        JsonCover jsonCover2 = userService.createUser(userJsonTest2);
        TestCase.assertNotNull(jsonCover2);
        TestCase.assertFalse(jsonCover2.getResponseResult());


        when(userDaoMocked.findByEmail("test2@email.com")).thenReturn(new UserEntity());

        // same user email exist ...
        UserJson userJsonTest3 = new UserJson(1L, "testUsername", "testPassword", true, "test2@email.com",
                "testFirstname", "testSurname", "m", 20, "testDesc", "www.test.com", new Timestamp(new Date().getTime()),
                null);
        JsonCover jsonCover3 = userService.createUser(userJsonTest3);
        TestCase.assertNotNull(jsonCover3);
        TestCase.assertFalse(jsonCover3.getResponseResult());
    }

    @Test
    public void updateUserJsonInDbTest() {
        when(userDaoMocked.findOne(1L)).thenReturn(new UserEntity());
        when(userDaoMocked.findByEmail("test@email.com")).thenReturn(null);
        when(groupDaoMocked.findOne(anyLong())).thenReturn(new GroupEntity());

        when(bCryptPasswordEncoderMocked.encode(anyString())).thenReturn("testPassword");

        UserJson userJsonTest1 = new UserJson(1L, "testUsername", "testPassword", true, "test@email.com",
                "testFirstname", "testSurname", "m", 20, "testDesc", "www.test.com", new Timestamp(new Date().getTime()),
                null);
        JsonCover jsonCover = userService.updateUser(userJsonTest1.getUserId(), userJsonTest1);
        TestCase.assertNotNull(jsonCover);
        TestCase.assertTrue(jsonCover.getResponseResult());

        // same user email exist ...
        when(userDaoMocked.findByEmail("test2@email.com")).thenReturn(new UserEntity(3L, "testUsername", "testPassword", true, "test@email.com",
                "testFirstname", "testSurname", "m", 20, "testDesc", "www.test.com", new Timestamp(new Date().getTime()),
                null));

        UserJson userJsonTest2 = new UserJson(1L, "testUsername", "testPassword", true, "test2@email.com",
                "testFirstname", "testSurname", "m", 20, "testDesc", "www.test.com", new Timestamp(new Date().getTime()),
                null);
        JsonCover jsonCover2 = userService.updateUser(userJsonTest2.getUserId(), userJsonTest2);
        TestCase.assertNotNull(jsonCover2);
        TestCase.assertFalse(jsonCover2.getResponseResult());
    }

    @Test
    public void deleteUserJsonFromDbTest() {
        when(userDaoMocked.findOne(1L)).thenReturn(new UserEntity());
        JsonCover jsonCover = userService.deleteUser(1L);
        TestCase.assertNotNull(jsonCover);
        TestCase.assertTrue(jsonCover.getResponseResult());
    }

    @Test
    public void getOrderedUserJsonListFromDbTest() {
        List<UserEntity> userEntityList = new ArrayList<>();
        userEntityList.add(userEntityTest1);
        userEntityList.add(userEntityTest2);
        userEntityList.add(userEntityTest3);
        when(userDaoMocked.getAll("orderByAgeASC", 0, 20)).thenReturn(userEntityList);
        when(userMapperMocked.convertUserEntityToJson(userEntityTest1)).thenReturn(new UserJson(1L, "testUsername", "testPassword", true, "test@email.com",
                "testFirstname", "testSurname", "m", 20, "testDesc", "www.test.com", new Timestamp(new Date().getTime()),
                null));
        when(userMapperMocked.convertUserEntityToJson(userEntityTest2)).thenReturn(new UserJson(2L, "testUsername", "testPassword", true, "test@email.com",
                "testFirstname", "testSurname", "m", 20, "testDesc", "www.test.com", new Timestamp(new Date().getTime()),
                null));
        when(userMapperMocked.convertUserEntityToJson(userEntityTest3)).thenReturn(new UserJson(3L, "testUsername", "testPassword", true, "test@email.com",
                "testFirstname", "testSurname", "m", 20, "testDesc", "www.test.com", new Timestamp(new Date().getTime()),
                null));
        List<UserJson> userJsonList = userService.getAllUsers("orderByAgeASC", 1, 20);
        TestCase.assertNotNull(userJsonList);
        TestCase.assertEquals(3, userJsonList.size());
        TestCase.assertTrue((userJsonList.get(0)).getUserId() <= (userJsonList.get(1)).getUserId());
    }
}
