package pl.mydomain.portfolio.web;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.powermock.reflect.Whitebox;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import pl.mydomain.portfolio.domain.pojo.JsonCover;
import pl.mydomain.portfolio.domain.pojo.UserJson;
import pl.mydomain.portfolio.service.UserService;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

public class UserControllerTest {

    UserController userController;
    UserService userServiceMocked;
    MockMvc mockMvc;

    @Before
    public void setUp() {
        userServiceMocked = mock(UserService.class);
        userController = new UserController();
        Whitebox.setInternalState(userController, "userService", userServiceMocked);
        this.mockMvc = standaloneSetup(userController).alwaysExpect(status().isOk()).build();
    }

    @Test
    public void getUserTest() throws Exception {
        when(userServiceMocked.getUser(1L)).thenReturn(new UserJson(1L, "testUsername2", "testPassword", true, "test@email.com",
                "testFirstname", "testSurname", "m", 20, "testDesc", "www.test.com", new Timestamp(new Date().getTime()),
                null));
        mockMvc.perform(get("/api/user/1")).andExpect(jsonPath("$").exists());
        verify(userServiceMocked, times(1)).getUser(1L);
    }

    @Test
    public void getAllUserTest() throws Exception {
        UserJson userJson1 = new UserJson(1L, "testUsername2", "testPassword", true, "test@email.com",
                "testFirstname", "testSurname", "m", 20, "testDesc", "www.test.com", new Timestamp(new Date().getTime()),
                null);
        UserJson userJson2 = new UserJson(2L, "testUsername2", "testPassword", true, "test@email.com",
                "testFirstname", "testSurname", "m", 20, "testDesc", "www.test.com", new Timestamp(new Date().getTime()),
                null);
        List<UserJson> userJsonList = new ArrayList<>();
        userJsonList.add(userJson1);
        userJsonList.add(userJson2);
        when(userServiceMocked.getAllUsers("orderByUsernameASC", 1, 20)).thenReturn(userJsonList);
        mockMvc.perform(get("/api/user/orderByUsernameASC/1/20")).andExpect(jsonPath("$").isArray());
        verify(userServiceMocked, times(1)).getAllUsers("orderByUsernameASC", 1, 20);
    }

    @Test
    public void createUserTest() throws Exception {
        when(userServiceMocked.createUser(anyObject())).thenReturn(new JsonCover(null, "", "", true));
        UserJson userJson = new UserJson(1L, "testUsername2", "testPassword", true, "test@email.com",
                "testFirstname", "testSurname", "m", 20, "testDesc", "www.test.com", new Timestamp(new Date().getTime()),
                null);
        ObjectMapper objectMapper = new ObjectMapper();
        mockMvc.perform(post("/api/user")
                .content(objectMapper.writeValueAsBytes(userJson)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.responseResult").value(true));

        verify(userServiceMocked, times(1)).createUser(anyObject());
    }

    @Test
    public void updateUserTest() throws Exception {
        when(userServiceMocked.updateUser(anyLong(), anyObject())).thenReturn(new JsonCover(null, "", "", true));
        UserJson userJson = new UserJson(1L, "testUsername2", "testPassword", true, "test@email.com",
                "testFirstname", "testSurname", "m", 20, "testDesc", "www.test.com", new Timestamp(new Date().getTime()),
                null);
        ObjectMapper objectMapper = new ObjectMapper();
        mockMvc.perform(post("/api/user/1")
                .content(objectMapper.writeValueAsBytes(userJson)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.responseResult").value(true));

        verify(userServiceMocked, times(1)).updateUser(anyLong(), anyObject());
    }

    @Test
    public void deleteUserTest() throws Exception {
        when(userServiceMocked.deleteUser(1L)).thenReturn(new JsonCover(null, "", "", true));
        mockMvc.perform(delete("/api/user/1")).andExpect(jsonPath("$").exists());
        verify(userServiceMocked, times(1)).deleteUser(1L);
    }
}
