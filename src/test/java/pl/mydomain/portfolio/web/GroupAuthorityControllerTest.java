package pl.mydomain.portfolio.web;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.powermock.reflect.Whitebox;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import pl.mydomain.portfolio.domain.pojo.AuthorityJson;
import pl.mydomain.portfolio.domain.pojo.GroupJson;
import pl.mydomain.portfolio.domain.pojo.JsonCover;
import pl.mydomain.portfolio.service.GroupAuthorityService;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

public class GroupAuthorityControllerTest {

    GroupAuthorityController groupAuthorityController;
    GroupAuthorityService groupAuthorityServiceMocked;
    MockMvc mockMvc;

    @Before
    public void setUp() {
        groupAuthorityServiceMocked = mock(GroupAuthorityService.class);
        groupAuthorityController = new GroupAuthorityController();
        Whitebox.setInternalState(groupAuthorityController, "groupAuthorityService", groupAuthorityServiceMocked);
        this.mockMvc = standaloneSetup(groupAuthorityController).alwaysExpect(status().isOk()).build();
    }

    @Test
    public void getGroupTest() throws Exception {
        when(groupAuthorityServiceMocked.getGroupById(1L)).thenReturn(new GroupJson(1L, "testGroup", null, null));
        mockMvc.perform(get("/api/group/1")).andExpect(jsonPath("$").exists());
        verify(groupAuthorityServiceMocked, times(1)).getGroupById(1L);
    }

    @Test
    public void getAllGroupTest() throws Exception {
        GroupJson groupJson1 = new GroupJson(1L, "testGroup", null, null);
        GroupJson groupJson2 = new GroupJson(2L, "testGrou2", null, null);
        List<GroupJson> groupJsonList = new ArrayList<>();
        groupJsonList.add(groupJson1);
        groupJsonList.add(groupJson2);
        when(groupAuthorityServiceMocked.getAllGroups()).thenReturn(groupJsonList);
        mockMvc.perform(get("/api/group")).andExpect(jsonPath("$").isArray());
        verify(groupAuthorityServiceMocked, times(1)).getAllGroups();
    }

    @Test
    public void createGroupTest() throws Exception {
        when(groupAuthorityServiceMocked.createGroup(anyObject())).thenReturn(new JsonCover(null, "", "", true));
        GroupJson groupJson = new GroupJson(1L, "testGroupName", null, null);
        ObjectMapper objectMapper = new ObjectMapper();
        mockMvc.perform(post("/api/group")
                .content(objectMapper.writeValueAsBytes(groupJson)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.responseResult").value(true));

        verify(groupAuthorityServiceMocked, times(1)).createGroup(anyObject());
    }

    @Test
    public void updateGroupTest() throws Exception {
        when(groupAuthorityServiceMocked.updateGroup(anyLong(), anyObject())).thenReturn(new JsonCover(null, "", "", true));
        GroupJson groupJson = new GroupJson(1L, "testGroupName", null, null);
        ObjectMapper objectMapper = new ObjectMapper();
        mockMvc.perform(post("/api/group/1")
                .content(objectMapper.writeValueAsBytes(groupJson)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.responseResult").value(true));

        verify(groupAuthorityServiceMocked, times(1)).updateGroup(anyLong(), anyObject());
    }

    @Test
    public void deleteGroupTest() throws Exception {
        when(groupAuthorityServiceMocked.deleteGroupById(1L)).thenReturn(new JsonCover(null, "", "", true));
        mockMvc.perform(delete("/api/group/1")).andExpect(jsonPath("$").exists());
        verify(groupAuthorityServiceMocked, times(1)).deleteGroupById(1L);
    }

    @Test
    public void getAllAuthorityTest() throws Exception {
        AuthorityJson authorityJson1 = new AuthorityJson(1L, "testAuth");
        AuthorityJson authorityJson2 = new AuthorityJson(1L, "testAuth2");
        List<AuthorityJson> authorityJsonList = new ArrayList<>();
        authorityJsonList.add(authorityJson1);
        authorityJsonList.add(authorityJson2);
        when(groupAuthorityServiceMocked.getAllAuthorities()).thenReturn(authorityJsonList);
        mockMvc.perform(get("/api/group/authorities")).andExpect(jsonPath("$").isArray());
        verify(groupAuthorityServiceMocked, times(1)).getAllAuthorities();
    }

}
